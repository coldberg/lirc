#include "textio_universel_helper.h"
#include "textio_utf8_helper.h"
#include "textio_ansi_helper.h"
#include "textio_ucs2_helper.h"

using namespace textio;
using namespace textio::helper;

universal::universal (bool nest) :
	_decoder (NULL)
{	
}

universal::~universal () {
	if (_decoder)
		delete _decoder;
}

bool universal::decode_char (universal::byte_type const &_in, universal::idword_type &_out, int &_bits) {
	if (_decoder)
		return _decoder->decode_char (_in, _out, _bits);
	_header.push_back (_in);	
	switch (_header.size ()) {
		case 1 : {
			if (_header [0] < 0xFE && _header [0] != 0xEF) {
				_decoder = new helper::ansi ();
				return _decoder->decode_char (_in, _out, _bits);
			}
			return false;
		}

		case 2 : {
			switch (*reinterpret_cast<unsigned short *> (&_header [0])) {
				case 0xFEFF:				
					_decoder = new helper::ucs2 (false, true);
					return false;
				case 0xFFFE:				
					_decoder = new helper::ucs2 (true, true);
					return false;
				default:
					return false;
			}		
		}

		case 3: {
			if (_header [1] == 0xBB && _header [2] == 0xBF) {				
				_decoder = new helper::utf8 (true);
				return false;
			}
		}
	}
	throw bad_encoding_exception ("Malformed BOM, unable to identify encoding");
	return false;
}