@echo off
set base_dir=%CD%
set address=08120000

rem generate font list file
CD fonts
call generate_fonts.bat


cd ..
del file_list.txt

for /F "delims="  %%i in ('dir out /B /A-D') 		do echo out\%%i>>file_list.txt
for /F "tokens=*" %%i in (img_file_list.txt) 	do echo %%i>>file_list.txt

echo %CD%
apps\HEXGen.exe -m -b -a %address% -n file_list.txt
MOVE file_list.hex romfs.s19
DEL file_list.himg
REM del file_list.txt

REM flash size = 0x60000 = 393216 Bytes (0x4a00000 - 0x500000)

apps\hexinfo.exe -f romfs.s19 -s 458752 >romfs_size.txt
move stat.txt romfs_stat.txt
set address=

apps\dfutool.exe --add-srecord romfs.s19,0,PSP_ROMFS  --vendor-id 0x0483 --product-id 0xdf11 --version 0x0000 --write-dfuse romfs.dfu
::DEL romfs.s19
COPY romfs.dfu \\dc\Design\PSP3\Soft\STM32_ROMFS\romfs.dfu 

PAUSE