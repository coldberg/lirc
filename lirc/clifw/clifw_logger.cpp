#include "clifw_logger.h"

using namespace clifw;


logger::logger (int verbosity, std::ostream &output, std::istream &input) :
	_verbosity (verbosity),
	_output (output),
	_input (input)
{}

int logger::verbosity () const {
	return _verbosity;
}

logger &logger::verbosity (int const verbosity) {
	_verbosity = verbosity;
	return *this;
}

logger &logger::cout () {
	static logger _cout (WARNING, std::cout, std::cin);
	return _cout;
}

logger &logger::cerr () {
	static logger _cerr (ERROR, std::cerr, std::cin);
	return _cerr;
}

logger &logger::clog () {
	static logger _clog (DEBUG, std::clog, std::cin);
	return _clog;
}

logger &logger::pause () {
	info ("Press Enter to continue...\n");
	_input.sync ();
	_input.get ();
	return *this;
}

std::string logger::type_string (int type) {
#define __S(X) case X: return #X
	switch (type) {
		__S(FATAL  );
		__S(ERROR  );
		__S(INFO   );
		__S(WARNING);
		__S(DEBUG  );
	};
	return "UNKNOWN";
}

#if defined (_WIN64) || defined(_WIN32)
#include <Windows.h>
logger &logger::set_attr (unsigned short attr) {
	SetConsoleTextAttribute (GetStdHandle (&_output != &std::cout ? STD_ERROR_HANDLE : STD_OUTPUT_HANDLE), attr);	
	return *this;
}
#else
logger &logger::set_attr (unsigned short attr) {
	return *this;
}
#endif