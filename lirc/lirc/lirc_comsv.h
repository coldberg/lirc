#ifndef __LIRC_COMSV_H__
#define __LIRC_COMSV_H__

#include "../textio/textio.h"

namespace lirc { 

	template <typename CharType> struct comsv_traits : textio::line_parser_traits<CharType, ',', '"', '#', ';'> {};
	template <typename CharType> struct comsv_parser : textio::line_parser<CharType, comsv_traits> {};

	template <typename CharType = char, typename EncodingHelper = textio::helper::universal> using comsv_file = 
		textio::line_file<CharType, EncodingHelper, comsv_parser>;		
}

#endif
