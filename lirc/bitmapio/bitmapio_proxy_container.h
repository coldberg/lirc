#ifndef __BITMAPIO_PROXY_CONTAINER_H__
#define __BITMAPIO_PROXY_CONTAINER_H__

#include <stdexcept>

namespace bitmapio {

	template <typename Type>
		struct proxy_container {
			typedef Type value_type;
			typedef Type *ptr_type;			
			typedef ptr_type iterator;

			proxy_container (iterator const inBegin_, iterator const inEnd_);
			proxy_container (proxy_container const &inCont_);

			iterator begin () const ;
			iterator end () const ;
			std::size_t size () const ;

			value_type const &operator [] (std::size_t const index_) const ;
			value_type &operator [] (std::size_t const index_) ;

		private:
			ptr_type begin_, end_;
		};
}

#endif