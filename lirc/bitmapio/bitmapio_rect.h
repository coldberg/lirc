#ifndef __BITMAPIO_RECT_H__
#define __BITMAPIO_RECT_H__

#include "bitmapio_coord.h"
#include "bitmapio_size.h"
#include "bitmapio_coord_ops.hpp"

#include <algorithm>

namespace bitmapio {
	template <typename T>
		struct rect {
			typedef T value_type;
			typedef coord<T> coord_type;
			typedef size<T> size_type;

			rect ();
			rect (coord_type const &ul, coord_type const &lr);
			rect (size_type const &size);
			rect (rect<T> const &other_) = default;
			rect<T> &operator = (rect<T> const &) = default;

			bool operator == (rect<T> const &other_) const ;
			bool operator != (rect<T> const &other_) const ;

			rect<T> operator + (rect<T> const &other_) const ; 
			rect<T> operator - (rect<T> const &other_) const ;
			rect<T> operator * (rect<T> const &other_) const ;
			rect<T> operator / (rect<T> const &other_) const ;
			rect<T> operator % (rect<T> const &other_) const ;
			rect<T> operator | (rect<T> const &other_) const ;
			rect<T> operator & (rect<T> const &other_) const ;

			value_type width	() const ;
			value_type height	() const ;
			value_type top		() const ;
			value_type bottom	() const ;
			value_type left		() const ;
			value_type right	() const ;

			coord_type const &lower_right () const ;
			coord_type const &upper_left  () const ;
			coord_type const &origin      () const ;

			size_type   size () const ;
			T area () const ;

			rect<T> &set_origin    (coord_type const &inOrigin_) ;
			rect<T> &adjust_origin (coord_type const &inDelta_) ;

		private:
			coord_type upper_left_, lower_right_;
		};

		
	template <typename T>
		rect<T> make_rect (T const &_top, T const &_right, T const &_bottom, T const &_left) {
			return rect<T> ({_left, _top}, {_right, _bottom});
		}

}

#endif