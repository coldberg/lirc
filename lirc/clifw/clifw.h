#ifndef __CLIFW_H__
#define __CLIFW_H__

#include "clifw_auto_var.h"
#include "clifw_universal_cast.h"
#include "clifw_string_utils.h"
#include "clifw_logger.h"
#include "clifw_auto_var.h"
#include "clifw_arguments.h"
#include "clifw_exception.h"
#include "clifw_option.h"
#include "clifw_validator.h"

#include <string>
#include <map>
#include <algorithm>
#include <functional>
#include <vector>

namespace clifw {

	template <typename UserApp>
		struct app {
			typedef auto_var<std::string>						argument_type;	
			typedef arguments<UserApp>							argument_list_type;
			typedef std::string									meta_key_type, meta_value_type;
			typedef std::map<meta_key_type, meta_value_type>	meta_list_type;
			typedef logger										output_type;
			typedef option_type<UserApp>						option_type;

			typedef std::function<bool (option_type &, output_type &)> callback_type ;
			
			static int main (int const argc, 
				char const * const * const argv, 
				char const * const * const envp);

			virtual logger &log () ;
			virtual meta_list_type const &meta (void) const;
			virtual meta_value_type const &meta (meta_key_type const &key) const;
			virtual app &meta (meta_list_type const &);
			virtual app &meta (meta_key_type const &key, meta_value_type const &val);
			
			virtual option_type &option (std::string const &name, bool alias = false);
			virtual option_type const &option (std::string const &name, bool alias = false) const;
			virtual option_type &add_option	(std::string const &name, std::string const &alias = "");
			virtual option_type &add_argument (std::string const &name);

			virtual option_type &argument (int index);
			virtual option_type const &argument (int index) const;
			virtual bool check_arguments (bool throw_exception = true) const ;
			
		protected:
			virtual void app_prolog (output_type & = logger::clog ()) const ;
			virtual void app_epilog (output_type & = logger::clog ()) const ;
			virtual bool app_help (option_type &, output_type & = logger::clog ()) const ;
						
			app (UserApp &) ;	
			virtual ~app () ;

		private:			

			meta_list_type _meta;
			std::map<std::string, option_type> _options;			
			std::map<std::string, std::reference_wrapper<option_type>> _aliases;
			std::list<std::reference_wrapper<const option_type>> _unordered_options;
			std::vector<std::string> _arguments;
			UserApp &_user_app ;
			logger &_logger;
		};



}

#include "clifw.hpp"

#endif
