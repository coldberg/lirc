#ifndef __CLIFW_OPTION_HXX__
#define __CLIFW_OPTION_HXX__

namespace clifw {

		template <typename UserApp>
		option_type<UserApp>::option_type (app<UserApp> &app, std::string const &name, std::string const &alias):
			_app (app),
			_name (name),
			_alias (alias),
			_description (""),
			_flags (option_type::BOOLEAN_SWITCH),
			_value (0),
			_present (false),
			_argument (-1),
			_validator (validator_none ()),
			_callback ([] (option_type &, output_type &) -> bool { 
				return true ; 
			})
		{

		}

	template <typename UserApp>
		option_type<UserApp> &option_type<UserApp>::description (std::string const &_) {
			_description = _ ;
			return *this;
		}

	template <typename UserApp>
		option_type<UserApp> &option_type<UserApp>::flags (unsigned int _) {
			_flags = _ ;
			return *this;
		}

	template <typename UserApp>
		option_type<UserApp> &option_type<UserApp>::value (typename app<UserApp>::argument_type const &_, bool present) {
			if (_validator (_, true)) {
				_value = _ ;			
				_present = present && _callback (*this, _app.log ());			
			}
			return *this;
		}

	template <typename UserApp>
		option_type<UserApp> &option_type<UserApp>::callback (typename app<UserApp>::callback_type const &_) {
			_callback = _ ;
			return *this;
		}

	template <typename UserApp>
		template <typename Class>
			option_type<UserApp> &option_type<UserApp>::callback (Class *_class, bool (Class::*_method) (typename Class::option_type &, typename Class::output_type &)) {
				return callback (std::bind (
					_method,
					_class,
					std::placeholders::_1,
					std::placeholders::_2));
			}

	
	template <typename UserApp>
		std::string const &option_type<UserApp>::name () const {			
			return _name; 
		}

	template <typename UserApp>
		std::string const &option_type<UserApp>::alias () const {			
			return _alias; 
		}

	template <typename UserApp>
		std::string const &option_type<UserApp>::description () const {			
			return _description; 
		}

	template <typename UserApp>
		unsigned int const &option_type<UserApp>::flags () const {			
			return _flags; 
		}

	template <typename UserApp>
		typename app<UserApp>::argument_type const &option_type<UserApp>::value () const {			
			return _value; 
		}

	template <typename UserApp>
		typename app<UserApp>::callback_type const &option_type<UserApp>::callback () const {			
			return _value; 
		}
	
	template <typename UserApp>
		bool const &option_type<UserApp>::present () const {			
			return _present; 
		}
	
	template <typename UserApp>
		bool option_type<UserApp>::check () const {
			return present () || flags () != REQUIRED_VALUE;				
		}

	template <typename UserApp>
		int option_type<UserApp>::takes_value () const {		
			return flags () != BOOLEAN_SWITCH ;
		}

	template <typename UserApp>
		int option_type<UserApp>::needs_value () const {		
			return flags () == REQUIRED_VALUE ;
		}
	template <typename UserApp>
		int const &option_type<UserApp>::argument () const {		
			return _argument;
		}
	template <typename UserApp>
		option_type<UserApp> &option_type<UserApp>::argument (int const &_) {		
			_argument = _;
			return *this;
		}

	template <typename UserApp>
		app<UserApp> const &option_type<UserApp>::parent () const {
			return _app;
		}

	template <typename UserApp>
		app<UserApp> &option_type<UserApp>::parent () {
			return _app;
		}

	template <typename UserApp>
		option_type<UserApp> &option_type<UserApp>::validator (validator_type const & _) {
			_validator = _ ;
			return *this;
		}
}

#endif