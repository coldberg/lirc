#ifndef __CLIFW_UNIVERSAL_CAST_H__
#define __CLIFW_UNIVERSAL_CAST_H__
				
#include <string>

namespace clifw {

	template <typename O, typename I>
		struct __lexical_cast;

	#define __MAKE_TOCSTRING_CAST(X)\
		template <>\
			struct __lexical_cast<std::string, X> {\
				static std::string _ (X const &in) {\
					return std::to_string (in);\
				}\
			}
	#define __MAKE_TOWSTRING_CAST(X)\
		template <>\
			struct __lexical_cast<std::wstring, X> {\
				static std::wstring _ (X const &in) {\
					return std::to_wstring (in);\
				}\
			}
		
	__MAKE_TOCSTRING_CAST (float				);
	__MAKE_TOCSTRING_CAST (double				);
	__MAKE_TOCSTRING_CAST (long double			);
	__MAKE_TOCSTRING_CAST (char					);
	__MAKE_TOCSTRING_CAST (unsigned char		);
	__MAKE_TOCSTRING_CAST (int					);
	__MAKE_TOCSTRING_CAST (unsigned int			);
	__MAKE_TOCSTRING_CAST (long					);
	__MAKE_TOCSTRING_CAST (unsigned long		);
	__MAKE_TOCSTRING_CAST (long long			);
	__MAKE_TOCSTRING_CAST (unsigned long long	);
	__MAKE_TOCSTRING_CAST (bool					);

	__MAKE_TOWSTRING_CAST (float				);
	__MAKE_TOWSTRING_CAST (double				);
	__MAKE_TOWSTRING_CAST (long double			);
	__MAKE_TOWSTRING_CAST (char					);
	__MAKE_TOWSTRING_CAST (unsigned char		);
	__MAKE_TOWSTRING_CAST (int					);
	__MAKE_TOWSTRING_CAST (unsigned int			);
	__MAKE_TOWSTRING_CAST (long					);
	__MAKE_TOWSTRING_CAST (unsigned long		);
	__MAKE_TOWSTRING_CAST (long long			);
	__MAKE_TOWSTRING_CAST (unsigned long long	);
	__MAKE_TOWSTRING_CAST (bool					);

	template <typename U, unsigned N>
		struct __lexical_cast<std::basic_string<U>, const U [N]> {
			static std::basic_string<U> _ (const U (&in) [N]) {
				return in;
			}
		};

	#define __MAKE_FROMSTRING_CASTF(X, F)\
		template <typename I>\
			struct __lexical_cast<X, std::basic_string<I>> {\
				static X _ (std::basic_string<I> const &in) {\
					return (X)F (in);\
				}\
			}
	#define __MAKE_FROMSTRING_CASTI(X, F)\
		template <typename I>\
			struct __lexical_cast<X, std::basic_string<I>> {\
				static X _ (std::basic_string<I> const &in) {\
					return (X)F (in, 0, 0);\
				}\
			}

	__MAKE_FROMSTRING_CASTF (float				, std::stof);
	__MAKE_FROMSTRING_CASTF (double				, std::stod);
	__MAKE_FROMSTRING_CASTF (long double		, std::stold);
	__MAKE_FROMSTRING_CASTI (char				, std::stol);
	__MAKE_FROMSTRING_CASTI (unsigned char		, std::stoul);
	__MAKE_FROMSTRING_CASTI (wchar_t			, std::stol);
	__MAKE_FROMSTRING_CASTI (int				, std::stoi);
	__MAKE_FROMSTRING_CASTI (unsigned int		, std::stoul);
	__MAKE_FROMSTRING_CASTI (long				, std::stol);
	__MAKE_FROMSTRING_CASTI (unsigned long		, std::stoul);
	__MAKE_FROMSTRING_CASTI (long long			, std::stoll);
	__MAKE_FROMSTRING_CASTI (unsigned long long	, std::stoull);
	__MAKE_FROMSTRING_CASTI (bool				, 0 != std::stoi);

	template <typename O, typename I>
		struct __universal_cast;

	template <typename C>
		struct __universal_cast<std::basic_string<C>, std::basic_string<C>> {
			static std::basic_string<C> _ (std::basic_string<C> const &in) {
				return in;
			}
		};

	template <typename O, typename I>
		struct __universal_cast<std::basic_string<O>, I> {
			static std::basic_string<O> _ (I const &in) {
				return __lexical_cast<std::basic_string<O>, I>::_ (in);
			}
		};

	template <typename O, typename I>
		struct __universal_cast<O, std::basic_string<I>> {
			static O _ (std::basic_string<I> const &in) {
				return __lexical_cast<O, std::basic_string<I>>::_ (in);
			}
		};

	template <typename U>
		struct __universal_cast<U, U> {
			static U _ (U const &in) {
				return in;
			}
		};
	

	#define __MAKE_UNIVERSAL_CXX_CAST(O, I, F)\
		template<>\
			struct __universal_cast<O, I> {\
				static O _ (I const &in) {\
					return F(in);\
				}\
			}

	__MAKE_UNIVERSAL_CXX_CAST (int,				float,			static_cast<int>	); 
	__MAKE_UNIVERSAL_CXX_CAST (int,				double,			static_cast<int>	); 
	__MAKE_UNIVERSAL_CXX_CAST (int,				long double,	static_cast<int>	); 
	__MAKE_UNIVERSAL_CXX_CAST (float,			int,			static_cast<float>	); 
	__MAKE_UNIVERSAL_CXX_CAST (double,			int,								); 
	__MAKE_UNIVERSAL_CXX_CAST (long double,		int,								); 

	// To be extended

	template <typename O, typename I>
		O universal_cast (I const &in) {
			return __universal_cast<O, I>::_ (in);
		}

			

}

#endif