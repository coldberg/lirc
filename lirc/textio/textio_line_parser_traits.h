#ifndef __TEXTIO_LINE_PARSER_TRAITS_H__
#define __TEXTIO_LINE_PARSER_TRAITS_H__

namespace textio {
	
	template <typename CharType, CharType SeparatorChar, CharType EscapeChar, CharType CommentChar, CharType AltSeparatorChar = SeparatorChar>
		struct line_parser_traits {
			typedef CharType								char_type;

			static std::uint_fast32_t const char_type_size				= sizeof (char_type) * 8;

			static char_type const separator_char			= SeparatorChar;
			static char_type const alt_separator_char		= AltSeparatorChar;
			static char_type const escape_char				= EscapeChar;
			static char_type const comment_char				= CommentChar;
		
		};

}

#endif