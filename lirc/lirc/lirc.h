#ifndef __LIRC_APP_H__
#define __LIRC_APP_H__


#include "../clifw/clifw.h"

#include "lirc_translation_resource.h"
#include "lirc_font_reference.h"

#include <cstdint>
#include <vector>
#include <filesystem>
#include <list>
#include <initializer_list>

namespace lirc {
	struct app : clifw::app<app> {
		typedef translation_resource::char_type			char_type;
		typedef std::basic_string<char_type>			string_type;
		typedef font_reference							font_reference_type;
		typedef font_reference::font_id_type			font_id_type;
		typedef translation_resource::charset_type		charset_type;
		typedef translation_resource::font_charset_type font_charset_type;
		typedef translation_resource::font_set_type		font_set_type;
		typedef std::vector<translation_resource>		translation_resource_collection_type;
		typedef std::vector<font_reference_type>		font_reference_collection_type;
		typedef std::map<font_id_type, font_id_type>	font_map_type;
		typedef std::map<font_id_type, std::string>		font_list_type;
		typedef font_reference::font_size_type			font_size_type;

		app ();
		~app ();

		int main (argument_list_type const &, output_type &) ;

		font_reference const &get_font (font_id_type const &_fid) const;
		font_reference &get_font (font_id_type const &_fid);
		font_set_type const &get_working_font_set () const;
		
		font_id_type font_remap_id (font_id_type const &) const;
		string_type	 remap_string (font_id_type const &_fid, string_type const &) const;
		

	protected:	

		app &add_font_list (std::string const &_path);
		app &add_translation (std::string const &_path);

		app &add_font (
			std::string const &_path, 
			std::string const &_size,
			std::string const &_default_char);

		app &add_native_font (
			std::string const &_path,
			std::string const &_charmap);

		app &fixup_include_glyph_range (
			std::string const &_fid, 
			std::string const &_start, 
			std::string const &_end);

		app &fixup_include_glyphs (
			std::string const &_fid, 
			std::list<std::string> const &_specifc);

		app &fixup_override_glyph_range (
			std::string const &_fid, 
			std::string const &_from_fid,
			std::string const &_rx, 
			std::string const &_ry);

		app &fixup_override_glyphs (
			std::string const &_fid, 
			std::string const &_from_fid,
			std::list<std::string> const &_glyphs);

		app &fixup_override_glyph_bitmap (
			std::string const &_fid,
			std::string const &_glyph,
			std::string const &_path,
			std::string const &_bitmap);

		app &fixup_per_font_padding (
			std::string const &_fid,
			std::string const &_top,
			std::string const &_right,
			std::string const &_bottom,
			std::string const &_left);

		app &fixup_baseline_shift_glyphs (
			std::string const &_shift,
			std::string const &_fontid,
			std::list<std::string> const &_glyphs);

        app &fixup_per_glyph_padding (
            std::string const &s_fid_, 
            std::string const &s_pleft_, 
            std::string const &s_pright_, 
            std::list<std::string> const &s_glyphs_);

	protected:
		app &add_char (font_id_type const &_fid, char_type const &_chr);
		app &add_char (font_id_type const &_fid, charset_type const &_chrl);
		app &inject_char (font_id_type const &_fid, font_id_type const &_from_fid, char_type const &_chrl);
		app &inject_char (font_id_type const &_fid, font_id_type const &_from_fid, charset_type const &_chrl);
		app &inject_bitmap (font_id_type const &_fid, char_type const &_chr, std::string const &_path, std::int_fast32_t  const &_baseline = -1);
		app &for_each_font (std::function<void (font_reference &_fref, font_id_type const &_fid)> const &, bool working = true);
		app &for_each_font_id (std::function<void (font_id_type const &_fid)> const &_ffun, bool _working = true);

		std::string locate_font (std::string const &path) const;
	private:
		translation_resource_collection_type	_translation_resources;
		font_reference_collection_type			_font_reference_set;
		font_set_type							_working_font_set;
		font_set_type							_total_font_set;
		font_set_type							_native_font_set;
		font_map_type							_font_map;
		font_list_type							_font_name_list;
		
	};
}


#endif