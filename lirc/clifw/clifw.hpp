#ifndef __CLIFW_HXX__
#define __CLIFW_HXX__

namespace clifw {
	template <typename UserApp>
		app<UserApp>::app (UserApp &user_app) :
			_meta ({
				{"name",		"clifw"},
				{"description",	"Command line framework for C++"},
				{"version",		"0.0." },
				{"build",		__TIME__ " " __DATE__},
				{"author",		"Aleksandr Shevchenko"},
				{"year",		"2014"},
			}),
			_user_app (user_app),
			_logger (logger::clog ())			
		{
			add_option ("help", "?")
				.description ("Prints help screen")
				.flags (option_type::OPTIONAL_VALUE)
				.callback (std::bind (
					&app<UserApp>::app_help,
					this,
					std::placeholders::_1,
					std::placeholders::_2
				));
			add_option ("version")
				.description ("Shows build date and git reference")
				.flags (option_type::OPTIONAL_VALUE)
				.callback ([&] (option_type &_help, output_type &_output) {
					_output.info ("Build: %s\n\n", meta ("build").c_str ());
					throw exception_template<0, 0> ("Done");
					return true;
				});
				
		}

	template <typename UserApp>
		app<UserApp>::~app () {

		}


	template <typename UserApp>
		typename app<UserApp>::meta_list_type const & app<UserApp>::meta (void) const {
			return _meta;
		}

	template <typename UserApp>
		app<UserApp> &app<UserApp>::meta (meta_list_type const &_) {
			_meta = _;
			return *this;
		}

	template <typename UserApp>
		app<UserApp> &app<UserApp>::meta (meta_key_type const &key, meta_value_type const &val) {
			_meta [key] = val;
			return *this;
		}

	template <typename UserApp>
		typename app<UserApp>::meta_value_type const &app<UserApp>::meta (
			typename app<UserApp>::meta_key_type const & key) const
		{
			return _meta.at (key);
		}


	template <typename UserApp>
		int app<UserApp>::main (int const argc, char const * const * const argv, char const * const * const envp)  try {			
			int ret = -128;
			UserApp user_app;
			logger &log = user_app.log ();
			
			try  {								
				user_app.app_prolog ();
				arguments<UserApp> arguments (user_app, argc, argv, envp);
				ret = user_app.main (arguments, log);
			}
			catch (::clifw::exception const &e) {
				if (e.code () == 0)
					return 0;
				log.set_attr (0xCF);
				log.write (e.type (), "%s", e.what ());
				log.set_attr ();
				log.write (e.type (), "\n\n");
				if (e.code () > -32 && e.code () < 0)
					user_app.option ("help").value ("", true);
				ret = e.code () ;
			}
			catch (std::exception const & e) {
				log.set_attr (0xCF);
				log.fatal ("GURU MEDITATION: %s", e.what ());
				log.set_attr ();
				log.fatal ("\n");
			}
			user_app.app_epilog () ;
			return ret;
		}
		catch (::clifw::exception const &e) {
			logger &log = logger::cerr ();
			log.set_attr (0xCF);
			log.write (e.type (), "%s", e.formatted ());
			log.set_attr ();
			log.fatal ("\n");
			return e.code () ;
		}
		catch (std::exception const &e) {
			logger &log = logger::cerr ();
			log.set_attr (0xCF);
			log.fatal ("GURU MEDITATION: %s", e.what ());
			log.set_attr ();
			log.fatal ("\n");
			return -128;
		}


	template <typename UserApp>
		logger &app<UserApp>::log () {
			return _logger;
		}

	template <typename UserApp>
		void app<UserApp>::app_prolog (output_type &out) const {
			out.info ("%s %s - %s\n\n", 
				meta ("name").c_str (),
				meta ("version").c_str (),
				meta ("description").c_str ());				
		}

	template <typename UserApp>
		void app<UserApp>::app_epilog (output_type &out) const {
			out.info ("\nCopyright (c) %s %s All Rights Reserved.\n",
				meta ("author").c_str (),
				meta ("year").c_str ());
				
		}

	template <typename UserApp>
		typename app<UserApp>::option_type &app<UserApp>::add_option (
			std::string const &name, 
			std::string const &alias) 
		{			
			_options.insert (std::make_pair (
				name, app<UserApp>::option_type (*this, name, alias)));			
			
			option_type &_opt = option (name) ;
			_unordered_options.push_back (std::cref (_opt));
			if (alias != "" ) {
				_aliases.insert (std::make_pair (
					alias, std::ref (_opt)));
			}
			return _opt;

		}

	template <typename UserApp>
		typename app<UserApp>::option_type &app<UserApp>::option (std::string const &name, bool alias) try {
			if (name == "1") __debugbreak ();
			return alias ? _aliases.at (name) : _options.at (name);
		}
		catch (std::out_of_range const &) {
			throw undefined_option_exception (helper::sprintf_format_string ("Unknown %s '%s'", alias ? "alias" : "option", name.c_str ()));
		}

	template <typename UserApp>
		typename app<UserApp>::option_type const &app<UserApp>::option (std::string const &name, bool alias) const try {
			if (name == "1") __debugbreak ();
			return alias ? _aliases.at (name) : _options.at (name);
		}
		catch (std::out_of_range const &) {
			throw undefined_option_exception (helper::sprintf_format_string ("Unknown %s '%s'", alias ? "alias" : "option", name.c_str ()));
		}

	template <typename UserApp>
		typename app<UserApp>::option_type &app<UserApp>::add_argument (std::string const &name) {
			_arguments.push_back (name);
			return add_option (name).argument (static_cast<int>(_arguments.size ()) - 1);
		}

	template <typename UserApp>
		typename app<UserApp>::option_type &app<UserApp>::argument (int index) try {			
			return option (_arguments.at (index));
		}
		catch (std::out_of_range const &) {
			throw value_unexpected_exception (helper::sprintf_format_string (
				"Too many arguments (%d)", index+1));
		}

	template <typename UserApp>
		typename app<UserApp>::option_type const &app<UserApp>::argument (int index) const try {			
			return option (_arguments.at (index));
		}
		catch (std::out_of_range const &) {
			throw value_unexpected_exception (helper::sprintf_format_string (
				"Too many arguments (%d)", index+1));
		}
			

	template <typename UserApp>
		bool app<UserApp>::check_arguments (bool throw_exception) const {
			for (auto const &i : _options) {
				auto const &o = i.second;
				if (o.check ())
					continue;
				if (!throw_exception) 
					return false;
				auto ai = std::find (_arguments.begin (), _arguments.end (), o.name ());	
				
				if (ai == _arguments.end ()) {
					throw option_required_exception (helper::sprintf_format_string (
						"Option '%s' is required.", o.name ().c_str ()));
				}

				throw too_few_arguments_exception (helper::sprintf_format_string (
					"Argument '%s' is required.", o.name ().c_str ()));
			}
			return true;		
		}

	template <typename UserApp>
		bool app<UserApp>::app_help (option_type &_help, output_type &_output) const {
			if (std::string (_help.value ()) != "") {			 
				auto const &_opt = option (std::string (_help.value ()));
				_output.info ("%s%s %s\n", 
					((_opt.argument () < 0 ? "--" : "" ) + _opt.name ()).c_str (), 
					(_opt.alias () != "" ? ", -" + _opt.alias () : "").c_str (), 
					_opt.description ().c_str ());
				return true;
			}

			_output.info ("USAGE: %s ", meta ("name").c_str ());
			int maxlen = 0;
			for (option_type const &opt: _unordered_options) {
				
				if (opt.name () == "help" || opt.alias () == "?")
					continue;
				auto const _needs_value = opt.needs_value () ;
				auto const _present = opt.present ();
				std::string fmt = _needs_value && !_present ? "<%s%s%s%s>" : "[%s%s%s%s]";
				_output.info (fmt + " ", 
					opt.argument () < 0 ? "--" : "", 
					opt.name ().c_str (),
					opt.argument () < 0 ? (opt.takes_value () ? "=value" : "[=value]") : "",
					opt.alias () != "" ? (" | -"+opt.alias ()+ (opt.takes_value () ? " value" : " [value]")).c_str () : ""
				);
				int totallen = static_cast<int>(opt.name ().size () + opt.alias ().size ()) ;
				totallen += (opt.name ().size () ? 2 : 0);
				totallen += (opt.alias ().size () ? 3 : 0);
				maxlen = maxlen >= totallen ? maxlen : totallen;
			}
			_output.info ("\n");
			
			std::string fmt = ("  %"+std::to_string (maxlen)+"s ");
			for (option_type const &opt: _unordered_options) {					
				_output.info (fmt, 
					((opt.argument () < 0 ? ("--"+opt.name ()).c_str () : helper::sprintf_format_string (opt.needs_value () ? "<%s>" : "[%s]", opt.name ().c_str ())) +
					(opt.alias () != "" ? (", -"+opt.alias ()).c_str () : std::string (""))).c_str ()
				);
				std::string _defv = opt.value ();
				_output.info (" %s\n" + (opt.present () && _defv != "" ? fmt + "\n" : "") + "\n", 
					opt.description ().c_str (),					
					opt.present () ? (" [default: "+_defv+"]").c_str () : ""
				);
			
				
			}
			throw exception ("help", 0, 0);

			return true;
		}
}

#endif