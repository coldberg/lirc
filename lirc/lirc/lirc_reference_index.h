#ifndef __LIRC_REFERENCE_INDEX_H__
#define __LIRC_REFERENCE_INDEX_H__

#include <tuple>
#include <list>

namespace lirc {
	
	template <typename ReferenceKeyType>
		struct reference_index {
			typedef ReferenceKeyType reference_key_type;
			typedef std::tuple<std::string, int> reference_type;
			typedef std::list<reference_type> reference_list_type;

			reference_list_type const &find_references (reference_key_type const &) const;
			std::string find_reference_string (reference_key_type const &, std::string const &_sep = "\n") const;

			reference_index &register_reference (reference_key_type const &, reference_type const &);
			template <typename Iter>
				reference_index &register_reference (Iter _begin, Iter _end, reference_type const &_ref) {
					for (auto i = _begin; i != _end; ++i)
						register_reference (*i, _ref);
					return *this;
				}

			static reference_index &get ();

		private:
			reference_index ();
			std::map<reference_key_type, reference_list_type> _references;
		};


}

#endif