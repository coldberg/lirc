#ifndef __CLIFW_STRING_UTILS_H__
#define __CLIFW_STRING_UTILS_H__

#include <string>
#include <cstring>
#include <vector>
#include <regex>
#include <functional>
#include <initializer_list>
#include <cctype>

#ifdef _MSC_VER
#define snprintf _snprintf
#endif

namespace clifw {

	namespace helper {

		template <typename... Args>
			std::string sprintf_format_string (std::string const &fmt, Args... args) {
				std::vector<char> buffer (0x1000, 0);
				size_t size = snprintf (&buffer [0], buffer.size (),  fmt.c_str (), args...);
				if (size >= buffer.size ()) {
					buffer.resize (size+1);
					snprintf (&buffer [0], buffer.size (), fmt.c_str (), args...);
				}
				return &buffer [0];
			}


		template <typename CType>
			std::basic_string<CType> string_toupper (std::basic_string<CType> const &_in) {								
				std::basic_string<CType> _buff (_in.begin (), _in.end ());
				std::transform (_in.begin (), _in.end (), _buff.begin (), toupper);
				return _buff;
			}

		template <typename CType>
			std::basic_string<CType> string_tolower (std::basic_string<CType> const &_in) {				
				std::basic_string<CType> _buff (_in.begin (), _in.end ());
				std::transform (_in.begin (), _in.end (), _buff.begin (), tolower);
				return _buff;
			}

		template <typename CType> 
			std::basic_string<CType> file_type (std::basic_string<CType> const &_path) {
				auto const _ext = string_tolower (std::tr2::sys::path (_path).extension ());			
				return _ext;
			}

	}
}

#endif