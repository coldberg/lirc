#ifndef __TEXTIO_ANSI_HELPER_H__
#define __TEXTIO_ANSI_HELPER_H__

#include <cstdint>

#include "textio_exceptions.h"
#include "textio_universel_helper.h"

namespace textio {
	namespace helper {
		struct ansi : universal {
			typedef std::uint8_t			byte_type;
			typedef std::int32_t			idword_type;

			ansi ();

			bool decode_char (byte_type const &_in, idword_type &_out, int &_bits);
			
		} ;	
	}
}

#endif