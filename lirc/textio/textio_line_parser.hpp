﻿#ifndef __TEXTIO_LINE_PARSER_HPP__
#define __TEXTIO_LINE_PARSER_HPP__

#include <vector>
#include <cstdint>

namespace textio {


	template <typename CharType, template <typename> class LineParserTraits>
		std::size_t line_parser<CharType, LineParserTraits>::parse_line (
			typename line_parser<CharType, LineParserTraits>::line_type const &_line,
			typename line_parser<CharType, LineParserTraits>::line_entry_type &_out,
			typename line_parser<CharType, LineParserTraits>::line_reference_type const &_lref)
		{
			_out.line_reference (_lref);

			static char_type const _sepch = separator_char;
			string_type _col;

			unsigned i = 0;
			bool _now_escape = false;
			

			// Clear up empty lines, trim beginning
			while (is_separator (_line [i])) {
				if (is_comment (_line [i]) || 
					(++i >= _line.size ()))
					return 0;
			}
			
			for (;i < _line.size (); ++i) {			
				char_type const &_lch = i > 0					? _line [i-1] : _sepch;
				char_type const &_cch = _line [i];
				char_type const &_rch = i + 1 < _line.size ()	? _line [i+1] : _sepch;

				if (!_now_escape) {
					if (is_comment (_cch)) {
						if (is_data (_lch)) {
							_out.push_back (_col);
							_col.clear ();
						}
						break;
					}

					if (is_escape (_cch)) {
						if (is_separator (_lch)) {
							_now_escape = true;
							continue;
						}

						throw bad_syntax_exception ("Unescaped delimiter");
					}

					if (is_separator (_cch) && !is_separator (_lch) && !is_escape (_lch)) {					
						_out.push_back (_col);
						_col.clear ();
						continue;
					}

					if (!is_separator (_cch))
						_col.push_back (_cch);
				}	
				else {
					if (is_escape (_cch)) {		
						//if (__examine_this) 
							//i = i;
						if (is_separator (_rch) || is_comment (_rch)) {
							_now_escape = false;
							_out.push_back (_col);
							_col.clear ();
							continue;
						}

						if (is_escape (_cch) && is_escape (_rch)) {
							_col.push_back (_cch);
							++i;
							continue;
						}

						throw bad_syntax_exception ("Unescaped delimiter");
					}					
					_col.push_back (_cch);
				}
				
			}

			if (_col.size ()) {
				_out.push_back (_col);
			}
			auto s = _out.size ();
			if (s != 0 && s != 2) {
//				__debugbreak ();
			}
			return _out.size () ;
		}


	template <typename CharType, template <typename> class LineParserTraits>
		bool line_parser<CharType, LineParserTraits>::is_break (
			typename line_parser<CharType, LineParserTraits>::char_type const &_chr)
		{
			return (_chr == lf_char || _chr == cr_char);
		}

	template <typename CharType, template <typename> class LineParserTraits>
		bool line_parser<CharType, LineParserTraits>::is_comment (
			typename line_parser<CharType, LineParserTraits>::char_type const &_chr)
		{
			return _chr == comment_char;
		}

	template <typename CharType, template <typename> class LineParserTraits>
		bool line_parser<CharType, LineParserTraits>::is_escape (
			typename line_parser<CharType, LineParserTraits>::char_type const &_chr)
		{
			return _chr == escape_char;
		}

	template <typename CharType, template <typename> class LineParserTraits>
		bool line_parser<CharType, LineParserTraits>::is_separator (
			typename line_parser<CharType, LineParserTraits>::char_type const &_chr)
		{
			return (_chr == separator_char || _chr == alt_separator_char);
		}

	template <typename CharType, template <typename> class LineParserTraits>
		bool line_parser<CharType, LineParserTraits>::is_data (
			typename line_parser<CharType, LineParserTraits>::char_type const &_chr)
		{
			return _chr && 
				!is_separator (_chr) && 
				!is_escape (_chr) && 
				!is_comment (_chr) && 
				!is_break (_chr);
		}

}

#endif