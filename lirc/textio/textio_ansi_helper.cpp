#include "textio_ansi_helper.h"

namespace textio {

	namespace helper {
		ansi::ansi ()
		{}

		bool ansi::decode_char (byte_type const &_chr, idword_type &_out, int &_bits) {
			_bits = 8;
			_out = _chr;
			return true;
		}		
	}
}