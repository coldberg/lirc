#ifndef __TEXTIO_UCS2_HELPER_H__
#define __TEXTIO_UCS2_HELPER_H__

#include <cstdint>
#include "textio_exceptions.h"
#include "textio_universel_helper.h"

namespace textio {
	namespace helper {
		struct ucs2 : universal {		
			ucs2 (bool swap = false, bool skipbom = true) ;
			bool decode_char (byte_type const &_in, idword_type &_out, int &_bits);
		private:
			bool _swap;
			bool _emit;
		};
	}
}

#endif