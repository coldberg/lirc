#include "lirc_resource_generator.h"
#include "lirc.h"

using namespace lirc;

resource_generator::resource_generator (app &lirc_app) :
	_lirc_app (lirc_app)
{}

resource_generator &resource_generator::write (std::string const &_fname, translation_resource const &_tres) {
	std::ofstream _res (_fname, std::ios::binary);

	auto const write = [&] (void const *_data, std::size_t size) {
		_res.write (reinterpret_cast<char const *>(_data), size);
		return !!_res;
	};

	auto const &_items = _tres.items ();
	std::uint32_t _nres = std::uint32_t (_items.size ());	
	std::uint32_t _coff = 2*sizeof (std::uint32_t);

	std::vector<std::uint32_t> _offsets;
	std::list<std::vector<std::uint8_t>> _chunks;
	for (auto const &_rln : _items) {
		_offsets.push_back (_coff);
		auto _text = _lirc_app.remap_string (_rln.font_id (), _rln.text ());
		std::uint16_t _fid = std::uint16_t (_lirc_app.font_remap_id (_rln.font_id ()));		
		std::uint32_t _csz = std::uint32_t ((_text.size () + 1) *sizeof (_text [0]));
		std::vector<std::uint8_t> _buffer (_csz + sizeof (_fid));
		memcpy (&_buffer [0], &_fid, sizeof (_fid));
		memcpy (&_buffer [2], _text.c_str (), _csz);
		_chunks.push_back (_buffer);
		_coff += (_csz + sizeof (_fid));
	}

	write (&_nres, sizeof (_nres));
	write (&_coff, sizeof (_coff));
	for (auto const &_chunk : _chunks) 
		write (&_chunk [0], _chunk.size ());		
	write (&_offsets [0], _offsets.size ()*sizeof (_offsets [0]));
	
	return *this;
}


resource_generator &resource_generator::write (std::string const &_fname, font_list_type const &_items) {
	std::ofstream _res (_fname, std::ios::binary);

	auto const write = [&] (void const *_data, std::size_t size) {
		_res.write (reinterpret_cast<char const *>(_data), size);
		return !!_res;
	};

	std::uint32_t _nres = std::uint32_t (_items.size ());	
	std::uint32_t _coff = 2*sizeof (std::uint32_t);

	std::vector<std::uint16_t> _offsets;

	for (auto const &_rln : _items) {
		_offsets.push_back (std::uint16_t (_coff));
		_coff += std::uint32_t ((_rln.size () + 1) * sizeof (_rln [0]));
	}	

	write (&_nres, sizeof (_nres));
	write (&_coff, sizeof (_coff));

	for (auto const &_rln : _items) {
		write (_rln.c_str (), (_rln.size () + 1)*sizeof (_rln [0]));
	}

	write (&_offsets [0], sizeof (_offsets [0]) * _offsets.size ());
	return *this;
}