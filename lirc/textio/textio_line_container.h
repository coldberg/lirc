#ifndef __TEXTIO_LINE_CONTAINER_H__
#define __TEXTIO_LINE_CONTAINER_H__

#include <vector>
#include <tuple>

namespace textio {
	template <typename T, class Alloc = std::allocator<T>> 
		struct line_container : public std::vector<T, Alloc>{
			typedef std::tuple<std::string, int> line_reference_type;

			line_container () {}

			line_container &line_reference (line_reference_type const & _lref) {
				_line_reference = _lref;
				return *this;
			}

			line_reference_type const &line_reference () const {
				return _line_reference;
			}
			
		private:
			line_reference_type _line_reference;
		};

}

#endif