#ifndef __TEXTIO_EXCEPTIONS_H__
#define __TEXTIO_EXCEPTIONS_H__

#include "../clifw/clifw_exception.h"

namespace textio {

	typedef clifw::exception_template<-64> bad_encoding_exception;	
	typedef clifw::exception_template<-65> char_overflow_exception;
	typedef clifw::exception_template<-66> bad_syntax_exception;
	typedef clifw::exception_template<-67> end_of_stream_exception;

}

#endif