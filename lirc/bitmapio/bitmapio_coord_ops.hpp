#ifndef __BITMAPIO_COORD_OPS_H__
#define __BITMAPIO_COORD_OPS_H__

#include <type_traits>
#include <cstdint>
namespace bitmapio {
	namespace detail {	
		template <typename InX, typename InW>
			inline auto align_ceil (InX const &_x, InW const &_w) -> decltype ((_x + _w - 1) / _w) {
				 return (_x + _w - 1) / _w;
			}

		template <typename InX, typename InW>
			inline auto align_floor (InX const &_x, InW const &_w) -> decltype (_x/_w) {
				 return _x / _w;
			}

		template <typename InX, typename InW>
			inline auto align_round (InX const &_x, InW const &_w) -> decltype ((_x + _w/2) / _w) {
				 return (_x + _w/2) / _w;
			}

		template <typename M>
			inline typename std::enable_if<std::is_integral<M>::value, M>::type 
				mod_ (M const &v_, M const &m_) 
			{				
				return v_ % m_;
			}
				
		template <typename M>
			inline typename std::enable_if<std::is_floating_point<M>::value ,M>::type 
				mod_ (M const &v_, M const &m_) 
			{
				return std::fmod (v_, m_);
			}
	}

	template <typename InMin, typename InMax, typename InVal> 
		inline bool contains (InMin const &min_, InMax const &max_, InVal const &val_) {
			return val_ >= min_ && val_ < max_;
		}

	template <typename InRect, typename InCoord> 
		inline bool contains (InRect const &rect_, InCoord const &coord_) {
			return (contains (rect_.left (), rect_.right (), coord_.x ()) &&
				contains (rect_.top (), rect_.bottom (), coord_.y ()));
		}

	template <typename Out, typename InMin, typename InMax, typename InX>
		inline Out clamp (InMin const &min_, InMax const &max_, InX const &x_) {
			return static_cast<Out> (x_ <= min_ ? min_ : (x_ >= max_ ? max_ :  x_)) ;
		}

	template <typename Rect, typename Coord>
		inline Coord clamp (Rect const &rect_, Coord const &coord_) {
			return Coord (
				clamp<Coord::value_type> (rect_.left (), rect_.right (), coord_.x ()),
				clamp<Coord::value_type> (rect_.top (), rect_.bottom (), coord_.y ()));
		}

	template <typename Out, typename InMin, typename InMax, typename InX>
		inline Out wrap (InMin const &min_, InMax const &max_, InX const &x_) {
			auto const x0_ = x_ - min_;
			auto const w0_ = max_ - min_;
			return static_cast<Out> (x0_ >= 0 ? mod_ (x0_, w0_) : w0_ - mod_ (-x0_, w0_));
		}
		
	template <typename Rect, typename Coord>
		inline Coord wrap (Rect const &rect_, Coord const &coord_) {
			return Coord (
				wrap<Coord::value_type> (rect_.left (), rect_.right (), coord_.x ()),
				wrap<Coord::value_type> (rect_.top (), rect_.bottom (), coord_.y ()));
		}

	template <typename Rect, typename Callback> 	
		inline void each_coord (Rect const &rect_, Callback const &callback_) {
			for (auto v = rect_.top (); v < rect_.bottom (); ++v) {
				for (auto u = rect_.left (); u < rect_.right (); ++u) {
					callback_ ({u, v}); 
				}			
			}
		}

	template <typename InMin, typename InMax, typename InX>
		inline InX invert (InMin const &inMin_, InMax const &inMax_, InX const &inX_, bool invert_) {						
			return invert_ ? static_cast<InX> (inMax_ - inX_ + inMin_ - 1) : inX_;
		}

	namespace invert_flags {
		static const std::uint32_t x = 1;
		static const std::uint32_t y = 2;
		static auto const both = x|y;
	}

	template <typename InRect, typename InCoord>
		inline InCoord invert (InRect const &inRect_, InCoord const &inCoord_, std::uint32_t flags) {			
			return flags ? InCoord (
				invert (inRect_.left (), inRect_.right (), inCoord_.x (), !!(flags & invert_flags::x)),
				invert (inRect_.top (), inRect_.bottom (), inCoord_.y (), !!(flags & invert_flags::y))):
				inCoord_;
		}

}

#endif