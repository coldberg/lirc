#ifndef __LIRC_UTILS_H__
#define __LIRC_UTILS_H__

namespace lirc {
	namespace utils {
		template <typename T>
			struct range {
				typedef T value_type;
				range (value_type const &_a, value_type const &_b, bool inclusive = true);
				struct iterator {
					typedef typename range<T>::value_type value_type;
					typedef std::input_iterator_tag iterator_category;
					typedef void difference_type;
					typedef value_type *pointer;
					typedef value_type &reference;

					iterator (value_type const &_i) ;

					typename range<T>::value_type const &operator * ();
					typename range<T>::iterator &operator ++ ();
					typename range<T>::iterator &operator -- ();
					iterator operator ++ (int);
					iterator operator -- (int);
					bool operator > (iterator const &_iter) const ;
					bool operator < (iterator const &_iter) const ;
					bool operator >= (iterator const &_iter) const ;
					bool operator <= (iterator const &_iter) const ;
					bool operator == (iterator const &_iter) const ;
					bool operator != (iterator const &_iter) const ;
				private:
					value_type _i;
				};

				iterator begin ();
				iterator end ();
				iterator begin () const;
				iterator end () const;

			private:
				value_type _a, _b;

			};

		template <typename T> 
			range<T>::range (value_type const &_a, value_type const &_b, bool inclusive = true):
				_a (_a),
				_b (_b + (inclusive ? value_type (1) : value_type (0)))
			{}

	
		template <typename T>
			typename range<T>::value_type const &range<T>::iterator::operator * () {
				return _i;
			}

		template <typename T>
			typename range<T>::iterator &range<T>::iterator::operator ++ () {
				++_i;
				return *this;
			}
		template <typename T>
			typename range<T>::iterator &range<T>::iterator::operator -- () {
				--_i;
				return *this;
			}

		template <typename T>
			typename range<T>::iterator range<T>::iterator::operator ++ (int) {
				iterator i = *this;										 
				++(*this);												 
				return i;												 
			}															 
																	 
		template <typename T>											 
			typename range<T>::iterator range<T>::iterator::operator -- (int) {
				iterator i = *this;
				--(*this);
				return i;
			}

		template <typename T>
			typename range<T>::iterator range<T>::begin () {
				return iterator (_a);
			}

		template <typename T>
			typename range<T>::iterator range<T>::end () {
				return iterator (_b);
			}

		template <typename T>
			typename range<T>::iterator range<T>::begin () const {
				return iterator (_a);
			}

		template <typename T>
			typename range<T>::iterator range<T>::end () const {
				return iterator (_b);
			}

		template <typename T>
			bool range<T>::iterator::operator < (typename range<T>::iterator const &_iter) const {
				return _i < _iter._i;		  
			}								  
										  
		template <typename T>				  
			bool range<T>::iterator::operator > (typename range<T>::iterator const &_iter) const {
				return _i > _iter._i;		
			}

		template <typename T>
			bool range<T>::iterator::operator <= (typename range<T>::iterator const &_iter) const {
				return _i <= _iter._i;
			}

		template <typename T>
			bool range<T>::iterator::operator >= (typename range<T>::iterator const &_iter) const {
				return _i >= _iter._i;
			}

		template <typename T>
			bool range<T>::iterator::operator == (typename range<T>::iterator const &_iter) const {
				return _i == _iter._i;
			}

		template <typename T>
			bool range<T>::iterator::operator != (typename range<T>::iterator const &_iter) const {
				return _i != _iter._i;
			}


		template <typename T>
			range<T>::iterator::iterator (typename range<T>::value_type const &_i) :
				_i (_i)
			{}

		template <typename T>
			range<T> make_range (T const &_a, T const &_b, bool inclusive = true) {
				return range<T> (_a, _b, inclusive);
			}


		namespace __abs {
			template <bool _issigned>
				struct helper {
					template <typename T>
						static inline T _ (T const &_xval);
				};
				
			template <>
			template <typename T>
				T helper<true>::_ (T const &_xval) {
					return _xval >= 0 ? _xval : -_xval;
				}

			template <>
			template <typename T>
				T helper<false>::_ (T const &_xval) {
					return _xval;
				}
		
		}

		template <typename T>
			T abs (T const & _val) {
				return __abs::helper<std::is_signed<T>::value>::_ (_val);				
			}
	}
}

#endif
