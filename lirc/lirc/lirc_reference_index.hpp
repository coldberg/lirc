#ifndef __LIRC_REFERENCE_INDEX_HPP__
#define __LIRC_REFERENCE_INDEX_HPP__

#include "lirc_reference_index.h"
#include <numeric>

namespace lirc {
	template <typename ReferenceKeyType>
		reference_index<ReferenceKeyType>::reference_index () {
		}

	template <typename ReferenceKeyType>
		reference_index<ReferenceKeyType> &reference_index<ReferenceKeyType>::get () {
			static reference_index<ReferenceKeyType> _ref;
			return _ref;
		}

	template <typename ReferenceKeyType>
		reference_index<ReferenceKeyType> &
			reference_index<ReferenceKeyType>::register_reference (
				reference_key_type const &_key, 
				reference_type const &_ref) 
		{
			_references [_key].push_back (_ref);
			return *this;
		}

	template <typename ReferenceKeyType>
		typename reference_index<ReferenceKeyType>::reference_list_type const &
			reference_index<ReferenceKeyType>::find_references (
				reference_key_type const &_key) const 		
		try {
			return _references.at (_key);
		}
		catch (std::exception const &) {
			static const reference_list_type _empty;
			return _empty;
		}
		
	template <typename ReferenceKeyType>
		std::string reference_index<ReferenceKeyType>::find_reference_string (reference_key_type const &_key, std::string const &_sep) const {
			auto const _ref_list = find_references (_key);
			std::list<std::string> _ref_str_list;
			for (auto const &_ref : _ref_list) {
				_ref_str_list.push_back (::clifw::helper::sprintf_format_string ("%s:%d", std::get<0>(_ref).c_str (), std::get<1> (_ref)));
			}
			if (!_ref_str_list.size ()) {
				return "Unknown location";
			}
			
			return std::accumulate (++_ref_str_list.begin (), _ref_str_list.end (), *_ref_str_list.begin (), [&] (std::string _a, std::string _b) {
				return _a + _sep + _b;
			});
		}
	
}

#endif