#ifndef __LIRC_TILSV_TRAITS_H__
#define __LIRC_TILSV_TRAITS_H__

#include "../textio/textio.h"

namespace lirc {
	template <typename CharType> struct tilsv_traits : textio::line_parser_traits<CharType, '~', '"', '\0', '~'> {};
	template <typename CharType> struct tilsv_parser : textio::line_parser<CharType, tilsv_traits> {};

	template <typename CharType = char, typename EncodingHelper = textio::helper::universal> using tilsv_file = 
		textio::line_file<CharType, EncodingHelper, tilsv_parser>;
		
}

#endif
