#ifndef __TEXTIO_UTF8_HELPER_H__
#define __TEXTIO_UTF8_HELPER_H__

#include <cstdint>
#include "textio_exceptions.h"
#include "textio_ansi_helper.h"
#include "textio_universel_helper.h"

namespace textio {
	namespace helper {
		struct utf8 : universal {

			utf8 (bool = false);

			bool decode_char (byte_type const &_in, idword_type &_out, int &_bits);

		private:
			std::uint_fast64_t _proc;
			int _bom;
			int _counter;
			int _utf8;		
			ansi _ansi;
		} ;	
	}
}

#endif