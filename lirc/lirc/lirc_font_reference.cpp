#include "lirc_font_reference.h"
#include "lirc_glyph_reference.h"
#include "lirc_exceptions.h"
#include "lirc_comsv.h"
#include "../clifw/clifw_validator.h"
#include "../clifw/clifw_string_utils.h"

using namespace lirc;

font_reference::font_reference (std::string const &__path, font_size_type size, std::string const &__name, char_type const &__default_char, std::string const &cmpath) :
	_name (__name != "" ? __name : std::tr2::sys::path (__path).filename ()),
	_path (__path),
	_size (size),
	_height (size),
	_is_native (_aux_is_native (__path)),
	_default_char (__default_char)
{
	int _line_ctr = 0;

	auto _charmap_path = cmpath ;

	if (_charmap_path != "" && clifw::validator::file_not_empty () (_charmap_path, false)) try {		
		auto const _items = comsv_file<char> (_charmap_path).items ();		
		for (auto const &_cmit : _items) {
			++_line_ctr;
			auto const _gid = clifw::universal_cast<wchar_t> (_cmit.at (0));
			auto const _cid = clifw::universal_cast<wchar_t> (_cmit.at (1));			
			_charmap [_gid] = _cid;
		}
	}
	catch (std::exception const &) {
		throw bad_charmap_exception ("%s(%d) Malformed charmap", _charmap_path.c_str (), _line_ctr);
	}
}

font_reference::font_reference (std::string const &_path, std::string const &_name, std::string const &_charmap) :
	font_reference (_path, -1, _name, '-', _charmap)
{
}

std::string const &font_reference::name () const {
	return _name;
}

std::string const &font_reference::path () const {
	return _path;
}

font_reference::font_size_type const &font_reference::size () const {
	if (is_native ()) {
		throw bad_font_op_exception ("Size not specified for pre-compiled fonts");
	}
	return _size;
}

font_reference &font_reference::inject_glyph (font_reference::char_type const &_char, glyph_reference const &_glyph) {
	if (is_native ()) {
		throw bad_font_op_exception ("Glyph injection not permitted on pre-compiled fonts");
	}
	_inject_glyphs.erase (_char);
	_inject_glyphs.insert (std::make_pair (_char, _glyph));
	return *this;
}

font_reference::glyph_map_type const &font_reference::patches () const {
	return _inject_glyphs;
}

font_reference::charset_type &font_reference::charset () {
	if (is_native ()) {
		throw bad_font_op_exception ("Charset retrieval not permitted on pre-compiled fonts");
	}
	return _charset;
}

font_reference::charset_type const &font_reference::charset () const {
	return _charset;
}

bool font_reference::_aux_is_native (std::string const &_path) {
	auto const _ext = clifw::helper::file_type (_path);
	return (_ext == ".f01" || _ext == ".f08" || _ext == ".ftd");
}

bool font_reference::is_native () const {
	return _is_native;
}

font_reference::char_map_type const &font_reference::charmap () const {
	return _charmap;
}

font_reference::char_map_type &font_reference::charmap () {
	return _charmap;
}

font_reference::char_map_type &font_reference::generate_charmap () {
	if (is_native ()) {
		throw bad_font_op_exception ("Charmap cannot be regenerated for pre-compiled fonts");
	}
    if (_charset.empty ()) {
        throw clifw::exception_template<56> ("Empty charset for font %s@%f",this->_name.c_str (), this->_size);
    }
	auto _base_char = *_charset.begin ();	
	for (auto const &_cid : _charset) {
		_charmap [_cid] = _base_char++;
	}
	return _charmap ;
}

font_reference::char_type const &font_reference::default_char () const {
	return _default_char;
}

font_reference &font_reference::height (font_size_type const &h) {
	_height = h;
	return *this;
} 

font_reference::font_size_type const &font_reference::height () const {	
	return _height;
} 

font_reference::rect_type const &
    font_reference::padding () const 
{
	return _padding;
}

font_reference &font_reference::padding (rect_type const &p) {
	if (is_native ()) {
		throw bad_font_op_exception ("Padding cannot be set for pre-compiled fonts.");
	}
	_padding = p;
	return *this;
}

font_reference &font_reference::baseline_shift_glyph (std::int32_t shift, char_type glyph) {
    if (is_native ()) {
		throw bad_font_op_exception ("Baseline shift cannot be set for pre-compiled fonts.");
	}
	_glyph_shift_map [glyph] = shift;
    return *this;
}

std::int32_t font_reference::baseline_shift_glyph (char_type g) const {
    auto si = _glyph_shift_map.find (g);
    return si != _glyph_shift_map.end () ? 
        si->second : 0;
}

font_reference::rect_type const &
    font_reference::local_padding (char_type g) const 
{
    static const auto empty_ = bitmapio::make_rect (0, 0, 0, 0);
	auto it = _glyph_padding_map.find (g);
    return it != _glyph_padding_map.end () ? it->second : empty_;
}

font_reference &font_reference::local_padding (char_type g, rect_type const &p) {
	if (is_native ()) {
		throw bad_font_op_exception ("Padding cannot be set for pre-compiled fonts.");
	}
	_glyph_padding_map [g] = p;
	return *this;
}