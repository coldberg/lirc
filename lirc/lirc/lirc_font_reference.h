#ifndef __LIRC_FONT_REFERENCE_H__
#define __LIRC_FONT_REFERENCE_H__

#include <string>
#include <filesystem>
#include <map>
#include <cstdint>

#include "../bitmapio/bitmapio_rect.h"
#include "lirc_glyph_reference.h"

namespace lirc {

	struct font_reference {
		typedef	glyph_reference::char_type				char_type;
		typedef glyph_reference::charset_type			charset_type;
		typedef glyph_reference::font_id_type			font_id_type;
		typedef glyph_reference::font_size_type			font_size_type ;		
        typedef bitmapio::rect<std::int32_t>            rect_type;
		typedef std::map<char_type, glyph_reference>	glyph_map_type;
		typedef std::map<char_type, char_type>			char_map_type;
        typedef std::map<char_type, std::int32_t>       glyph_shift_map_type;
        typedef std::map<char_type, rect_type>          glyph_pad_map_type;
				
		font_reference (std::string const &path, font_size_type size, std::string const &_name = "", char_type const &_default_char = '-', std::string const &_charmap = "");
		font_reference (std::string const &path,					  std::string const &_name = "", std::string const &_charmap = "");		

		font_reference &inject_glyph (char_type const &_char, glyph_reference const &_glyph);
        font_reference &baseline_shift_glyph (std::int32_t shift, char_type glyph);

        std::int32_t baseline_shift_glyph (char_type g) const;

		std::string const &name () const ;
		std::string const &path () const ;

		font_size_type const &size () const; 
		char_type const &default_char () const;
		font_size_type const &height () const;
		font_reference &height (font_size_type const &);

		bool is_native () const;

		charset_type &charset ();
		charset_type const &charset () const;
		char_map_type &charmap () ;
		char_map_type const &charmap () const;
		glyph_map_type const &patches () const;
		char_map_type &generate_charmap ();

		font_reference &padding (rect_type const &_padding);
		rect_type const &padding () const;
		font_reference &local_padding (char_type g, rect_type const &_padding);
		rect_type const &local_padding (char_type g) const;

	protected:
		bool _aux_is_native (std::string const &_path);

	private:
		std::string			            _name;
		std::string			            _path;
		font_size_type		            _size;
		font_size_type		            _height;
		glyph_map_type		            _inject_glyphs;
		charset_type		            _charset;
		bool				            _is_native;
		char_map_type		            _charmap;
		char_type			            _default_char;
		rect_type	                    _padding;
        glyph_shift_map_type            _glyph_shift_map;
        glyph_pad_map_type              _glyph_padding_map;
	};


}

#endif
