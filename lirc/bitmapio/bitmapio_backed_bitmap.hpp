#ifndef __BITMAPIO_BACKED_BITMAP_HPP__
#define __BITMAPIO_BACKED_BITMAP_HPP__

#include <vector>
#include "bitmapio_proxy_bitmap.h"

namespace bitmapio {
	
	template <typename WordType, std::uint32_t Flags = bitmap_flags::default_, template <typename T, typename = std::allocator<T>> class Container=std::vector> 
		struct backed_bitmap : proxy_bitmap<Container<WordType>, Flags> {						
			
			inline backed_bitmap (
				typename container_type::value_type const *inBegin_,
				rect_type const &inRect_, 
				std::size_t const &inPitch_):
				proxy_bitmap (
					{inBegin_, inBegin_ + bitmap_size (
						inRect_, inPitch_)}, 
					inRect_, inPitch_)
			{}

			inline backed_bitmap (
				rect_type const &inRect_, 
				std::size_t const &inPitch_):
				proxy_bitmap (
					container_type (bitmap_size (
						inRect_, inPitch_), 0), 
					inRect_, 
					inPitch_)
			{}

			template <typename InputBitmapType>
				inline backed_bitmap (InputBitmapType const &inBitmap_) :
					proxy_bitmap (
						container_type (bitmap_size (
							inBitmap_.rect (), 
							inBitmap_.pitch ()), 0),
						inBitmap_.rect (), 0)
				{
					superimpose (*this, inBitmap_);
				}

		};
	

}

#endif