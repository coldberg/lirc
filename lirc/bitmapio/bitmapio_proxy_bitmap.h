#ifndef __BITMAPIO_PROXY_BITMAP_H__
#define __BITMAPIO_PROXY_BITMAP_H__

#include <cstdint>
#include <memory>

#include "bitmapio_coord.h"
#include "bitmapio_rect.h"
#include "bitmapio_size.h"
#include "bitmapio_proxy_container.h"
#include "bitmapio_bitmap_ops.hpp"

namespace bitmapio {

	namespace bitmap_flags {
		static std::uint32_t const align					= 1 << 0;
		static std::uint32_t const msb_first				= 1 << 1;
		static std::uint32_t const little_endian			= 1 << 2;
		static std::uint32_t const invert_scanline_order	= 1 << 3;
		static std::uint32_t const no_align					= 0;
		static std::uint32_t const lsb_first				= 0;
		static std::uint32_t const big_endian				= 0;
		static std::uint32_t const normal_scanline_order	= 0;
		static std::uint32_t const default_					= bitmap_flags::align|bitmap_flags::msb_first|bitmap_flags::little_endian;
	}

	namespace detail {

		template <typename BitmapType> struct is_aligned 
			{enum {value = !!(BitmapType::flags & bitmap_flags::align)};};

		template <typename BitmapType> struct is_not_aligned 
			{enum {value = !(BitmapType::flags & bitmap_flags::align)};};

		template <typename BitmapType> struct is_little_endian 
			{enum {value = !!(BitmapType::flags & bitmap_flags::little_endian)};};

		template <typename BitmapType> struct is_big_endian 
			{enum {value = !(BitmapType::flags & bitmap_flags::little_endian)};};

		template <typename BitmapType> struct is_msb_first 
			{enum {value = !!(BitmapType::flags & bitmap_flags::msb_first)};};

		template <typename BitmapType> struct is_lsb_first 
			{enum {value = !(BitmapType::flags & bitmap_flags::msb_first)};};

		template <typename BitmapType> struct is_inverted_scanline_order
			{enum {value = !!(BitmapType::flags & bitmap_flags::invert_scanline_order)};};

		template <typename BitmapType> struct is_normal_scanline_order
			{enum {value = !(BitmapType::flags & bitmap_flags::invert_scanline_order)};};
	
	}

	template <typename ContainerType = proxy_container<std::uint8_t>, std::uint32_t Flags = bitmap_flags::default_>
		struct proxy_bitmap {
			typedef bool value_type;
			typedef rect<std::int32_t> rect_type;
			typedef rect<std::int32_t>::coord_type coord_type;
			typedef rect<std::int32_t>::size_type size_type;
			typedef ContainerType container_type;
			typedef typename ContainerType::value_type word_type;
			static auto const flags = Flags;
			static auto const word_size_in_bits = sizeof (word_type) << 3;
			
			proxy_bitmap (
				ContainerType	const &inCont_, 
				rect_type		const &inRect_, 
				std::size_t		const &inPitch_ = 0);

			value_type get (coord_type const &) const;
			proxy_bitmap &set (coord_type const &, value_type const);

			rect_type::value_type width () const ;
			rect_type::value_type height () const ;
			std::size_t pitch () const ;			

			rect_type const &rect() const ;
			coord_type const &origin () const;

			rect_type const &adjust_origin (coord_type const &inDelta_);
			rect_type const &set_origin (coord_type const &inOrigin_);
			
			size_type size () const;
			std::size_t length () const;
			container_type const &container () const;
			container_type &container ();
			
		protected:
			static std::size_t bitmap_pitch (rect_type const &inRect_, std::size_t const &inPitch_);
			static std::size_t bitmap_size (rect_type const &inRect_, std::size_t const &inPitch_);

		private:
			rect_type		rect_;
			std::size_t		pitch_;
			std::size_t		size_;
			ContainerType	container_;
		};

}

#endif