#ifndef __BITMAPIO_SIZE_H__
#define __BITMAPIO_SIZE_H__

#include "bitmapio_coord.h"

namespace bitmapio {

	template <typename T>
		struct size : coord<T> {
			size (T const &_w, T const &_h) ;
			T width  () const ;
			T height () const ;
			T area () const;
		};

}

#endif