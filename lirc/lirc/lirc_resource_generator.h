#ifndef __LIRC_RESOURCE_GENERATOR_H__
#define __LIRC_RESOURCE_GENERATOR_H__

#include "lirc_translation_resource.h"
#include "lirc_font_reference.h"

namespace lirc {
	
	struct app;
	
	struct resource_generator {
		typedef std::vector<translation_resource::string_type> font_list_type; 

		resource_generator (app &lirc_app);		
		resource_generator &write (std::string const &_file, translation_resource const &_tres);
		resource_generator &write (std::string const &_fname, font_list_type const &_flist);
	private :
		app &_lirc_app;
	};
	
}

#endif