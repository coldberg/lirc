#include "clifw_exception.h"
#include "clifw_string_utils.h"

using namespace clifw;

exception::exception (std::string const & reason, int code, int type) throw () :
	_formatted (helper::sprintf_format_string ("%s [exit code: %d]: %s", logger::type_string (type).c_str (), code, reason.c_str ())),
	_reason (reason),
	_code (code),
	_type (type)
{}

exception::~exception () throw () {}

char const * exception::what () const throw () {
	return _formatted.c_str () ;
}

std::string const &exception::formatted () const throw () {
	return _formatted;
}

std::string const &exception::reason () const throw () {
	return _reason;
}

int const &exception::type () const throw () {
	return _type;
}

int const &exception::code () const throw () {
	return _code;
}

