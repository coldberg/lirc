#include "textio_utf8_helper.h"
#include "textio_ansi_helper.h"

namespace textio {

	namespace helper {
		utf8::utf8 (bool skipbom) : 
			_proc (skipbom ? 3 : 0),
			_bom (skipbom ? 3 : 0),
			_counter (0),
			_utf8 (skipbom ? 1 : 0)				
		{}

		bool utf8::decode_char (byte_type const &_chr, idword_type &_out, int &_bits) {
			++_proc;
			if (_proc <= 4) {
				if (_proc == 1 && _chr == 0xEF) {
					++_bom;
					return false;
				}

				if (_proc == 2 && _chr == 0xBB) {
					++_bom;
					return false;
				}

				if (_proc == 3 && _chr == 0xBF) {
					++_bom;
					_utf8 = 1;
					return false;
				}

				if (_proc == 4 && (_bom != 0 && _bom != 3))							
					throw bad_encoding_exception ("Utf-8 BOM malformed.");

			}					

			if (!_utf8) {						
				return _ansi.decode_char (_chr, _out, _bits);
			}

			if (_counter  < 1) {
				if (_chr < 0x80) {
					_out = _chr;
					_bits = 7;
					return true;
				}
				else {
					_out = 0;

					if (_chr < 0xC0) {								
						throw bad_encoding_exception ("Bad utf-8 encoding.");
					}

					if (_chr < 0xE0) { // >= 0xC0 | 110xxxxx
						_out = _chr & 0x1F;
						_counter = 1;
						_bits = 11;								
						return false;
					}

					if (_chr < 0xF0) { // >= 0xE0 | 1110xxxx

						_out = _chr & 0xF;
						_counter = 2;
						_bits = 16;
						return false;
					}

					if (_chr < 0xF8) { // >= 0xF0 | 11110xxx
						_out = _chr & 0x7;
						_counter = 3;
						_bits = 21;
						return false;
					}

					if (_chr < 0xFC) { // >= 0xF8 | 111110xx
						_out = _chr & 0x3;
						_counter = 4;
						_bits = 26;
						return false;
					}

					if (_chr < 0xFE) { // >= 0xFC | 1111110x
						_out = _chr & 0x1;
						_counter = 5;
						_bits = 31;
						return false;
					}

					throw bad_encoding_exception ("Bad utf-8 encoding.");
				}
			}
			else {
				_out = (_out << 6) | (_chr & 0x3F);						
				--_counter;						
				return _counter < 1;
			}
		}	
	
	}
}