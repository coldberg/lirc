#include "clifw_validator.h"
#include <regex>
#include <filesystem>
#include <fstream>

using namespace clifw;
using namespace clifw::validator;

bool file_exists::operator() (auto_var<std::string> const &_value, bool throw_exception) {
	auto _path = std::tr2::sys::path (std::string (_value));
	bool _valid = std::tr2::sys::exists (_path) && !std::tr2::sys::is_directory (_path);	
	if (!throw_exception || _valid)
		return _valid;
	throw file_exists::exception (helper::sprintf_format_string ("File '%s' doesn't exist", std::string (_value).c_str ()));
}

bool file_not_empty::operator() (auto_var<std::string> const &_value, bool throw_exception) {
	bool _valid = file_exists () (_value, throw_exception) &&		
		std::tr2::sys::file_size (std::tr2::sys::path (std::string (_value))) > 0;
	if (!throw_exception || _valid)
		return _valid;
	throw file_not_empty::exception (helper::sprintf_format_string ("File '%s' is empty", std::string (_value).c_str ()));
}

bool integer_value::operator() (auto_var<std::string> const &_value, bool throw_exception) {
	bool _valid = std::regex_match (std::string (_value), std::regex ("^([-+]?[0-9]+|[-+]?0x[0-9a-fA-F]+)$"));
	if (!throw_exception || _valid)
		return _valid;
	throw integer_value::exception (helper::sprintf_format_string ("'%s' is not a valid integer", std::string (_value).c_str ()));
}

bool number_value::operator() (auto_var<std::string> const &_value, bool throw_exception) {
	bool _valid = integer_value () (_value, false) ||		
		std::regex_match (std::string (_value), std::regex ("^[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?$"));
	if (!throw_exception || _valid)
		return _valid;
	throw number_value::exception (helper::sprintf_format_string ("'%s' is not a valid number", std::string (_value).c_str ()));
}

in_list::in_list (std::initializer_list<auto_var<std::string>> const &ls) :
	_list (ls)
{}

bool in_list::operator() (auto_var<std::string> const &_value, bool throw_exception) {
	bool _valid = std::find (_list.begin (), _list.end (), _value) != _list.end ();
	if (!throw_exception || _valid)
		return _valid;
	throw number_value::exception (helper::sprintf_format_string ("'%s' is not in the list of valid values", std::string (_value).c_str ()));
}

bool file_writable::operator() (auto_var<std::string> const &_value, bool throw_exception) {
	auto _path = std::tr2::sys::path (std::string (_value));
	bool _valid = !std::tr2::sys::exists (_path)  || std::ofstream (std::string (_value).c_str (), std::ios::app|std::ios::binary).is_open ();
	if (!throw_exception || _valid)
		return _valid;
	throw file_writable::exception (helper::sprintf_format_string ("File '%s' isn't writable", std::string (_value).c_str ()));
}
