#ifndef __TEXTIO_LINE_PARSER_H__
#define __TEXTIO_LINE_PARSER_H__

#include <vector>
#include <string>
#include <utility>

#include "textio_line_container.h"

namespace textio {

	template <typename CharType, template <typename> class LineParserTraits>
		struct line_parser {
			typedef typename LineParserTraits<CharType>::char_type				char_type;
			typedef std::basic_string<char_type>								string_type;
			typedef std::basic_string<char_type>								line_type;
			typedef typename line_container<string_type>::line_reference_type	line_reference_type;
			typedef line_container<string_type>									line_entry_type;

			static std::uint_fast32_t const char_type_size						= LineParserTraits<CharType>::char_type_size;
			static char_type const separator_char					= LineParserTraits<CharType>::separator_char;
			static char_type const alt_separator_char				= LineParserTraits<CharType>::alt_separator_char;
			static char_type const escape_char						= LineParserTraits<CharType>::escape_char;			
			static char_type const comment_char						= LineParserTraits<CharType>::comment_char;

			static char_type const cr_char							= '\r';
			static char_type const lf_char							= '\n';

			static std::size_t parse_line (line_type const &_line, line_entry_type &_out, line_reference_type const &_lref);

			static bool is_break (char_type const &_chr);
			static bool is_separator (char_type const &_chr);
			static bool is_escape (char_type const &_chr);
			static bool is_comment (char_type const &_chr);
			static bool is_data (char_type const &_chr);
			
		};

}

#include "textio_line_parser.hpp"

#endif