import os
_fpath = os.path.dirname (os.path.abspath (__file__)) + '/build.txt';
if os.path.isfile (_fpath):
	_build = int (open (_fpath, 'r').read ()) + 1 ;
	open (_fpath, 'w').write ('{0:d}'.format (_build));
	open ('version_build.h', 'w').write ('#define __BUILD_NUMBER__ "{0:d}"'.format (_build));
else:
	open (_fpath, 'w').write ('0');
	open ('version_build.h', 'w').write ('#define __BUILD_NUMBER__ "0"');