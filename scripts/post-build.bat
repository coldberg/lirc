@echo off
xcopy /y %2\depends\freetype-2.5.3\%1\bin\freetype.dll %3
if not "%4" == "Debug" (
	xcopy /y %3*.exe %2NuGen\apps
	xcopy /y %3*.dll %2NuGen\apps	
	upx -9 %2NuGen\apps\lirc.exe 
)