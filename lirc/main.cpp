#include <stdexcept>
#include <iostream>
#include <fstream>

#include "clifw/clifw.h"
#include "lirc/lirc.h"
int main (int const argc, char const * const * const argv, char const * const * const envp) try {
	return clifw::app<lirc::app>::main (argc, argv, envp);	
}
catch (std::exception const &e) {
	clifw::logger::clog ().fatal ("%s\n", e.what ()) ;
	clifw::logger::clog ().pause () ;
}