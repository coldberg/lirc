#ifndef __LIRC_FONT_GENERATOR_H__
#define __LIRC_FONT_GENERATOR_H__

#include "../bitmapio/bitmapio_backed_bitmap.hpp"
#include "lirc_glyph_reference.h"
#include "lirc_font_reference.h"
#include "lirc_font_precursor.h"
#include "lirc_glyph_renderer.h"
#include "lirc_endian_helper.hpp"


namespace lirc {
	namespace font {
		static const auto flags_f01 = 
			bitmapio::bitmap_flags::no_align|
			bitmapio::bitmap_flags::little_endian|
			bitmapio::bitmap_flags::normal_scanline_order|
			bitmapio::bitmap_flags::lsb_first;

		static const auto flags_f08 = 
			bitmapio::bitmap_flags::align|
			bitmapio::bitmap_flags::little_endian|
			bitmapio::bitmap_flags::normal_scanline_order|
			bitmapio::bitmap_flags::msb_first;

		static const auto flags_ftd = 
			bitmapio::bitmap_flags::align|
			bitmapio::bitmap_flags::big_endian|
			bitmapio::bitmap_flags::normal_scanline_order|
			bitmapio::bitmap_flags::msb_first;

		typedef bitmapio::backed_bitmap<std::uint16_t, flags_f01> type_f01;
		typedef bitmapio::backed_bitmap<std::uint8_t, flags_f08> type_f08;
		typedef bitmapio::backed_bitmap<std::uint16_t, flags_ftd> type_ftd, type_f16;		

		template <typename FontType>
			struct generator {			
				typedef FontType								glyph_bitmap_type;
				typedef typename FontType::container_type		glyph_buffer_type;
				typedef typename FontType::word_type			word_type;
				typedef typename FontType::word_type			width_type;
				typedef std::uint16_t							offset_type;
				typedef glyph_reference::char_type				char_type;		
				typedef glyph_renderer							glyph_renderer_type;
				typedef font_precursor							font_precursor_type;

				generator (glyph_renderer_type &_gren, font_precursor_type const &_fprec, app &_app);
				generator &write (std::string const &_fpath);
				glyph_bitmap_type build_glyph (
					std::int32_t const &top_, 
					std::int32_t const &bot_, 
					glyph_bitmap const &inBitmap_, 
					bitmapio::rect<std::int32_t> const &inPadding_,
                    std::int32_t const &inShift_);

			private:
				template <typename Type>
					Type endian_fix (Type const &inWord_) {
						if (bitmapio::detail::is_big_endian<glyph_bitmap_type>::value)
							return lirc::helper::big_endian<Type>::fix (inWord_);
						return lirc::helper::lit_endian<Type>::fix (inWord_);
					}
				template <typename Type>
					void endian_fix_inplace (Type &ioWord_) {
						ioWord_ = endian_fix (ioWord_);
					}

				glyph_renderer_type &glyph_renderer_;
				font_precursor_type const &font_precursor_;
				app &app_;
				std::uint32_t min_width_;
				bool ascent_dont_care_;
			};

		template <typename FontType>
			void generate (std::string const &inPath_, typename generator<FontType>::font_precursor_type const &inPrecursor_, 
				app &inApp_, typename generator<FontType>::glyph_renderer_type &inRender_) ;
			/*
			{
				generator<FontType> (inRender_, inPrecursor_, inApp_).write (inPath_);
			}*/

	}
}

#endif