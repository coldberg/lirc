#ifndef __CLIFW_AUTO_VAR_H__
#define __CLIFW_AUTO_VAR_H__

#include <string>
#include <vector>
#include <map>
#include <memory>

#include "clifw_universal_cast.h"

namespace clifw {


	template <typename T = std::string>
		struct auto_var {
			typedef T value_type;

			auto_var (value_type const &in);
			auto_var<T> &operator = (T const &in);
			operator value_type const & () const;

			template <typename O>
				operator O () const;
			template <typename I>
				auto_var (I const &in);
			template <typename I>
				auto_var<T> &operator = (I const &in);

			bool operator == (auto_var<T> const &) const;
			bool operator > (auto_var<T> const &) const;
			bool operator < (auto_var<T> const &) const;
			bool operator >= (auto_var<T> const &) const;
			bool operator <= (auto_var<T> const &) const;
		private:
			T _value;				
		};

	template <typename T>
	template <typename I>
		auto_var<T>::auto_var (I const &in) :
			auto_var<T> (universal_cast<typename auto_var<T>::value_type> (in))
		{}

	template <typename T>
	template <typename O>
		auto_var<T>::operator O () const {
			return universal_cast<O> (_value);
		}

	template <typename T>
	template <typename I>
		auto_var<T> & auto_var<T>::operator = (I const &in) {
			return operator = (universal_cast<value_type> (in));
		}


	template <typename T>
		auto_var<T>::auto_var (value_type const &in) :
			_value (in)
		{}

	template <typename T>
		auto_var<T>::operator const typename auto_var<T>::value_type & () const {
			return _value;
		}

	template <typename T>
		auto_var<T> &auto_var<T>::operator = (value_type const &in) {
			_value = in;
			return *this;
		}

	template <typename T>
		bool auto_var<T>::operator == (auto_var<T> const &_) const {
			return _value == _._value;
		}

	template <typename T>
		bool auto_var<T>::operator > (auto_var<T> const &_) const {
			return _value > _._value;
		}

	template <typename T>
		bool auto_var<T>::operator < (auto_var<T> const &_) const {
			return _value < _._value;
		}
	template <typename T>
		bool auto_var<T>::operator >= (auto_var<T> const &_) const {
			return _value >= _._value;		
		}							
									
	template <typename T>			
		bool auto_var<T>::operator <= (auto_var<T> const &_) const {
			return _value <= _._value;
		}
}

#endif