#ifndef __LIRC_SPASV_TRAITS_H__
#define __LIRC_SPASV_TRAITS_H__

#include "../textio/textio.h"

namespace lirc {

	template <typename CharType> struct spasv_traits : textio::line_parser_traits<CharType, ' ', '"', '#', '\t'> {};
	template <typename CharType> struct spasv_parser : textio::line_parser<CharType, spasv_traits> {};

	template <typename CharType = char, typename EncodingHelper = textio::helper::universal> using spasv_file = 
		textio::line_file<CharType, EncodingHelper, spasv_parser>;

}

#endif
