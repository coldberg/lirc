#ifndef __LIRC_GLYPH_RENDERER_H__
#define __LIRC_GLYPH_RENDERER_H__

#include "lirc_font_reference.h"
#include "lirc_font_precursor.h"
#include "../bitmapio/bitmapio_backed_bitmap.hpp"

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

#include <map>

namespace lirc {
	typedef bitmapio::backed_bitmap<std::uint8_t> glyph_bitmap;

	struct app;
	struct glyph_reference;

	struct glyph_renderer {
		typedef glyph_reference::char_type									char_type;
		typedef glyph_reference::font_size_type								font_size_type;
		typedef glyph_reference::font_id_type								font_id_type;
		typedef std::set<font_id_type>										font_set_type;
		typedef std::vector<font_reference>									font_reference_collection_type;
		typedef std::map<font_id_type, std::map<FT_Long, FT_Face>>			face_map_type;		
		typedef std::map<font_id_type, std::map<char_type, glyph_bitmap>>	glyph_cache_type;
		typedef std::map<std::string, glyph_bitmap>							glyph_bitmap_cache_type;

		glyph_renderer (app &_app);
	   ~glyph_renderer ();

		glyph_bitmap const &get_glyph_bitmap (glyph_reference const &_gref);
		
		glyph_renderer &dump_glyphs (std::string const &_outdir, font_precursor const &_font_prec);				
		glyph_renderer &generate_font (std::string const &_outfile, font_precursor const &_font_prec, std::uint_fast32_t const &_font_type = 8);
	protected:
		glyph_bitmap const &get_glyph_bitmap (font_id_type const &_fid, char_type const &_glyph);
		glyph_bitmap const &get_glyph_bitmap (std::string const &path, std::int_fast32_t  const &baseline = -1);

		FT_Face &get_face (font_id_type const &_fid, FT_Long const &_faceidx = 0);
		font_reference const &get_font (font_id_type const &_fid) const;
	private:
		lirc::app								&lirc_app_;				
		face_map_type							face_map_;
		FT_Library								library_;
		glyph_cache_type						glyph_cache_;
		glyph_bitmap_cache_type					glyph_bitmap_cache_;
		bool									auto_crop_;		
	};

}

#endif