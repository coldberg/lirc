#ifndef __CLIFW_LOGGER_H__
#define __CLIFW_LOGGER_H__

#include "clifw_string_utils.h"
#include <iostream>

namespace clifw {

	struct logger {
	
		enum {
			FATAL,
			ERROR,
			INFO,
			WARNING,
			DEBUG			
		};

		template <typename... Args>
			logger &write (int const type, std::string const&fmt, Args... args) {
				if (type <= _verbosity) {
					std::cout << helper::sprintf_format_string (fmt, args...);
				}
				return *this;
			}
			
		template <typename... Args>
			logger &debug (std::string const&fmt, Args... args) {
				return write (DEBUG, fmt, args...);
			}

		template <typename... Args>
			logger &warning (std::string const&fmt, Args... args) {
				return write (WARNING, fmt, args...);
			}

		template <typename... Args>
			logger &info (std::string const&fmt, Args... args) {
				return write (INFO, fmt, args...);
			}

		template <typename... Args>
			logger &error (std::string const&fmt, Args... args) {
				return write (ERROR, fmt, args...);
			}

		template <typename... Args>
			logger &fatal (std::string const&fmt, Args... args) {
				return write (FATAL, fmt, args...);
			}

		logger &pause ();
		logger &set_attr (unsigned short attr = 0x07);		

		logger (int verbosity = WARNING, std::ostream &out = std::cout, std::istream &in = std::cin);
		int verbosity () const;
		logger &verbosity (int const);

		static logger &cout ();
		static logger &cerr ();
		static logger &clog ();
		static std::string type_string (int type);

	private:
		std::istream &_input;
		std::ostream &_output;
		int _verbosity;
		
	};

}


#endif