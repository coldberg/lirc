#ifndef __LIRC_TRANSLATION_RESOURCE_H__
#define __LIRC_TRANSLATION_RESOURCE_H__

#include <iterator>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <cstdint>

#include "lirc_exceptions.h"
#include "lirc_tilsv.h"
#include "lirc_spasv.h"
#include "lirc_glyph_reference.h"
#include "lirc_font_reference.h"

namespace lirc {
	struct translation_resource {

		typedef glyph_reference::charset_type			charset_type;
		typedef glyph_reference::font_set_type			font_set_type;
		typedef font_reference::char_type				char_type;
		typedef font_reference::font_id_type			font_id_type;
		typedef std::basic_string<char_type>			string_type;		
		typedef std::map<font_id_type, charset_type>	font_charset_type;

		//typedef std::vector<string_type> item_type;

		struct translation_element_type {

			font_id_type	_font_id;
			string_type		_text;
			charset_type	_char_set;

			translation_element_type (font_id_type font_id, string_type const &text):
				_font_id (font_id),
				_text (text)
			{
				_char_set.insert (_text.begin (), _text.end ());
			}

			charset_type const &charset () const {
				return _char_set;
			}

			font_id_type const &font_id () const {
				return _font_id;
			}

			string_type const &text () const {
				return _text;
			}
		};

		typedef std::vector<translation_element_type> item_collection_type;

		translation_resource (std::string const &_path);
		item_collection_type &items () ;
		item_collection_type const &items () const ;

		font_set_type const &font_set () const;
		charset_type const &font_charset (unsigned) const;

		std::string const &name () const;

	private:		
		std::string				_name;
		item_collection_type	_item_collection;
		font_set_type			_font_id_set;
		font_charset_type		_font_charset_map;
	};

}

#endif