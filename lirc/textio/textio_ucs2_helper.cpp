#include "textio_ucs2_helper.h"

using namespace textio;
using namespace textio::helper;

ucs2::ucs2 (bool swap, bool skipbom) :
	_swap (swap),
	_emit (false)
{}

bool ucs2::decode_char (byte_type const &_in, idword_type &_out, int &_bits) {
	if (!_emit) {
		_out = _in;
		_bits = 8;
		_emit = !_emit;
		return false;
	}
	else {
		_out = _swap ? 
			(_out << 8) | _in : 
			(_in << 8) | _out ;
		_bits = 16;
		_emit = !_emit;
		return true;
	}
}