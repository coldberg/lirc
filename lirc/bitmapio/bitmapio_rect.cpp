#include "bitmapio_rect.h"

namespace bitmapio {

	template <typename T>
		rect<T>::rect () 
		{}

	template <typename T>
		rect<T>::rect (coord_type const &ul, coord_type const &lr):
			upper_left_ (ul),
			lower_right_ (lr)
		{}

	template <typename T>
		rect<T>::rect (size_type const &size) :
			rect ({0, 0}, size)
		{
		}


	template <typename T>
		bool rect<T>::operator == (rect<T> const &other_) const {
			return upper_left_ == other_.upper_left_ && 
				lower_right_ == other_.lower_right_;
		}

	template <typename T>
		bool rect<T>::operator != (rect<T> const &other_) const {
			return upper_left_ != other_.upper_left_ && 
				lower_right_ != other_.lower_right_;
		}

	template <typename T>
		rect<T> rect<T>::operator + (rect<T> const &other_) const {
			return {upper_left_ + other_.upper_left_, lower_right_ + other_.lower_right_};
		}

	template <typename T>
		rect<T> rect<T>::operator - (rect<T> const &other_) const {
			return {upper_left_ - other_.upper_left_, lower_right_ - other_.lower_right_};
		}

	template <typename T>
		rect<T> rect<T>::operator * (rect<T> const &other_) const {
			return {upper_left_ * other_.upper_left_, lower_right_ * other_.lower_right_};
		}

	template <typename T>
		rect<T> rect<T>::operator / (rect<T> const &other_) const {
			return {upper_left_ / other_.upper_left_, lower_right_ / other_.lower_right_};
		}

	template <typename T>
		rect<T> rect<T>::operator % (rect<T> const &other_) const {
			return {upper_left_ % other_.upper_left_, lower_right_ % other_.lower_right_};
		}

	template <typename T>
		rect<T> rect<T>::operator | (rect<T> const &other_) const {			
			auto const top_		= std::min (top		(),	other_.top		());
			auto const left_	= std::min (left	(),	other_.left		());
			auto const bottom_	= std::max (bottom	(),	other_.bottom	());
			auto const right_	= std::max (right	(),	other_.right	());
			return rect<T> ({left_, top_}, {right_, bottom_});
		}

	template <typename T>
		rect<T> rect<T>::operator & (rect<T> const &other_) const {				
			auto const top_		= std::max (top		(),	other_.top		());
			auto const left_	= std::max (left	(),	other_.left		());
			auto const bottom_	= std::min (bottom	(),	other_.bottom	());
			auto const right_	= std::min (right	(),	other_.right	());
			return rect<T> ({left_, top_}, {right_, bottom_});
		}

	template <typename T>
		T rect<T>::width () const {
			return lower_right_.x () - upper_left_.x ();
		}

	template <typename T>
		T rect<T>::height () const {
			return lower_right_.y () - upper_left_.y ();
		}

	template <typename T>
		T rect<T>::top () const {
			return upper_left ().y ();
		}

	template <typename T>
		T rect<T>::bottom () const {
			return lower_right ().y ();
		}

	template <typename T>
		T rect<T>::left () const {
			return upper_left ().x ();
		}

	template <typename T>
		T rect<T>::right () const {
			return lower_right ().x ();
		}

	template <typename T>
		typename rect<T>::coord_type const &
			rect<T>::lower_right () const 
		{
			return lower_right_;
		}

	template <typename T>
		typename rect<T>::coord_type const &
			rect<T>::upper_left () const 
		{
			return upper_left_;
		}

	template <typename T>
		typename rect<T>::coord_type const &
			rect<T>::origin () const 
		{
			return upper_left () ;
		}

	template <typename T>
		typename rect<T>::size_type 
			rect<T>::size () const 
		{
			return size_type (width (), height ());
		}

	template <typename T>
		T rect<T>::area () const {
			return width () * height () ;
		}			

	template <typename T>
		rect<T> &rect<T>::set_origin (coord_type const &inOrigin_) {
			lower_right_ = inOrigin_ + size ();
			upper_left_ = inOrigin_ ;
			return *this;
		}

	template <typename T>
		rect<T> &rect<T>::adjust_origin (coord_type const &inDelta_) {
			lower_right_ = lower_right_ + inDelta_;
			upper_left_ = upper_left_ + inDelta_;
			return *this;
		}

	template struct rect<std::int8_t>;
	template struct rect<std::uint8_t>;
	template struct rect<std::int16_t>;
	template struct rect<std::uint16_t>;
	template struct rect<std::int32_t>;
	template struct rect<std::uint32_t>;
	template struct rect<std::int64_t>;
	template struct rect<std::uint64_t>;
	template struct rect<float>;
	template struct rect<double>;
}