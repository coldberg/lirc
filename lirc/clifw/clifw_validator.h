#ifndef __CLIFW_VALIDATOR_H__
#define __CLIFW_VALIDATOR_H__

#include "clifw_auto_var.h"
#include "clifw_exception.h"
#include <list>
#include <string>

namespace clifw {	
	namespace validator {		

		struct file_exists  {
			bool operator () (auto_var<std::string> const &value, bool throw_exception);
			typedef exception_template<-32> exception;
		};		

		struct file_not_empty {
			bool operator () (auto_var<std::string> const &value, bool throw_exception);
			typedef exception_template<-33> exception;
		};		

		struct integer_value {
			bool operator () (auto_var<std::string> const &value, bool throw_exception);
			typedef exception_template<-34> exception;		
		};

		struct number_value {
			bool operator () (auto_var<std::string> const &value, bool throw_exception);
			typedef exception_template<-35> exception;		
		};

		struct in_list {
			in_list (std::initializer_list<auto_var<std::string>> const &list);
			bool operator () (auto_var<std::string> const &value, bool);
			typedef exception_template<-36> exception;
		private:
			std::list<auto_var<std::string>> _list;
		};

		struct file_writable {
			bool operator () (auto_var<std::string> const &value, bool throw_exception);
			typedef exception_template<-37> exception;
		};		

		template <typename Lambda>
			struct lambda_validator {
				typedef exception_template<-38> exception;

				lambda_validator (Lambda const &inCallback_, std::string const &inMsg_ = "Bad parameter"):
					message_	(inMsg_),
					callback_	(inCallback_)
				{}

				bool operator () (auto_var<std::string> const &value, bool throw_exception) {
					if (callback_ (value))
						return true;
					if (throw_exception)
						throw exception (message_);
					return false;
				}

			private:
				std::string message_;
				std::function<bool (auto_var<std::string> const &)> callback_;
			};

		template <typename Lambda>
			lambda_validator<Lambda> 
				make_lambda_validator (Lambda const &inLambda_, std::string const &inMsg_ = "Bad parameter") 
			{
				return {inLambda_, inMsg_}; 
			}

	}
}

#endif