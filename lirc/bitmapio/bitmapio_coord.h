#ifndef __BITMAPIO_COORD_H__
#define __BITMAPIO_COORD_H__

#include <cmath>
#include <type_traits>
#include "bitmapio_coord_ops.hpp"

namespace bitmapio {
	template <typename T>
		struct coord  {

			typedef T value_type;

			coord (T x, T y);
			coord ();
			~coord () = default;
			coord (coord<T> const &) = default;	
			coord<T> &operator = (coord<T> const &) = default;
			bool operator == (coord<T> const &other_) const ;
			bool operator != (coord<T> const &other_) const ;

			coord<T> operator + (coord<T> const &other_) const ;
			coord<T> operator - (coord<T> const &other_) const ;
			coord<T> operator * (coord<T> const &other_) const ;
			coord<T> operator / (coord<T> const &other_) const ;
			coord<T> operator % (coord<T> const &other_) const ;

			T const &x () const ;
			T const &y () const ;

		private:
			T x_, y_;
		};



}

#endif