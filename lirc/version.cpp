#include "lirc/lirc_version.h"
#include "version_build.h"
#include "version_git.h"

#define __LIRC_VERSION__		"0.2." __GIT_COMMIT_COUNT__ "." __BUILD_NUMBER__
#define __LIRC_BUILD__			__BUILD_NUMBER__ " | " __GIT_CURRENT_REF__ " (" __GIT_CURRENT_BRANCH__ ") | "__BUILD_PLATFORM__ " (" __BUILD_CONFIGURATION__  ") | " __TIME__ " " __DATE__ ""

char const *__lirc_version__	= __LIRC_VERSION__ ;
char const *__lirc_build__		= __LIRC_BUILD__ ;
