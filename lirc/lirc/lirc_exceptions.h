#ifndef __LIRC_EXCEPTIONS_H__
#define __LIRC_EXCEPTIONS_H__

#include "../clifw/clifw_exception.h"

namespace lirc {

	typedef clifw::exception_template<1> bad_font_id_exception;
	typedef clifw::exception_template<2> bad_font_op_exception;
	typedef clifw::exception_template<3> bad_charmap_exception;
	typedef clifw::exception_template<4> filesystem_exception;
	typedef clifw::exception_template<5> freetype_exception;
	typedef clifw::exception_template<6> bad_bitmap_exception;
	typedef clifw::exception_template<7> internal_error_exception;
	typedef clifw::exception_template<8> bad_reference_exception;
	typedef clifw::exception_template<9> bad_command_exception;
}

#endif