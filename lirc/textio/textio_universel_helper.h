#ifndef __TEXTIO_UNIVERSAL_HELPER_H__
#define __TEXTIO_UNIVERSAL_HELPER_H__

#include <cstdint>
#include "textio_exceptions.h"



namespace textio {
	namespace helper {
		//FIXME : write a agregator for ucs16, utf8 and ansi helpers to autodetect and choose appropriate helper
		//typedef utf8 universal;

		struct universal {
			typedef std::uint8_t			byte_type;
			typedef std::int32_t			idword_type;			

			virtual bool decode_char (byte_type const &_in, idword_type &_out, int &_bits);

			universal (bool = false);
			~universal ();

		private:
			universal *_decoder;
			std::vector<byte_type> _header;
		};
	}
}
#endif