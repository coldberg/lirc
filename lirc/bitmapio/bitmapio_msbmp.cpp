#include "bitmapio_msbmp.hpp"
#include <fstream>

namespace bitmapio {
	namespace detail {
		#pragma pack(push, 1)

		typedef struct _WinBMPFileHeader {
			std::uint16_t file_type;			/* File type, always 4D42h ("BM") */
			std::uint32_t file_size;			/* Size of the file in bytes */
			std::uint16_t reserved1;			/* Always 0 */
			std::uint16_t reserved2;			/* Always 0 */
			std::uint32_t bitmap_offset;		/* Starting position of image data in bytes */
		} WINBMPFILEHEADER;

		typedef struct _Win2xBitmapHeader
		{
			std::uint32_t size;					/* Size of this header in bytes */
			std::int16_t  width;				/* Image width in pixels */
			std::int16_t  height;				/* Image height in pixels */
			std::uint16_t planes;				/* Number of color planes */
			std::uint16_t bits_per_pixel;		/* Number of bits per pixel */
		} WIN2XBITMAPHEADER;

		typedef struct _Win2xPaletteElement
		{
			std::uint8_t blue;					/* Blue component */
			std::uint8_t green;					/* Green component */
			std::uint8_t red;					/* Red component */
		} WIN2XPALETTEELEMENT;
		
		struct bmp_full_header {
			WINBMPFILEHEADER	h0;
			WIN2XBITMAPHEADER	h1;
			WIN2XPALETTEELEMENT pal [2];
		} ;

		struct bmp_partial_header {
			WINBMPFILEHEADER	h0;
			WIN2XBITMAPHEADER	h1;		
		} ;

		#pragma pack(pop)
		
		void write_bmp (std::string const &inPath_, bmp_bitmap const &inBitmap_) {
			std::ofstream bmpFile_ (inPath_, std::ios::binary);
			if (!bmpFile_) throw std::runtime_error (
				"Couldn't create bitmapf file: "+inPath_);

			auto const _store = inBitmap_.container () ;			
			auto const bmpDataSize_ = _store.size () * sizeof (_store [0]);
			
			detail::bmp_full_header header_;

			memset (&header_, 0, sizeof (header_));

			header_.h0.file_type		= 'B' | ('M' << 8);
			header_.h0.file_size		= std::uint32_t (sizeof (header_) + bmpDataSize_);
			header_.h0.bitmap_offset	= sizeof (header_);
			header_.h1.size				= sizeof (header_.h1);
			header_.h1.width			= static_cast<std::int16_t> (inBitmap_.width ());
			header_.h1.height			= static_cast<std::int16_t> (inBitmap_.height ());
			header_.h1.planes			= 1;
			header_.h1.bits_per_pixel	= 1;
			header_.pal [0]				= {0xff, 0xff, 0xff};
			bmpFile_.write (reinterpret_cast<char const *> (&header_), sizeof (header_));
			bmpFile_.write (reinterpret_cast<char const *> (&_store [0]), bmpDataSize_);		
		}

		bmp_bitmap read_bmp (std::string const &inPath_) {		
			detail::bmp_partial_header bmpHeader_;
			std::ifstream bmpFile_ (inPath_, std::ios::binary);
			auto const errMsg_ = "Couldn't read bitmap file: "+inPath_;
			if (!bmpFile_) throw std::runtime_error (
				errMsg_);
			bmpFile_.read (reinterpret_cast<char *>(&bmpHeader_), 
				sizeof (bmpHeader_));
			if (bmpFile_.gcount () != sizeof (bmpHeader_))
				throw std::runtime_error (errMsg_);
			if (bmpHeader_.h0.file_type != ('B' | ('M' << 8)))
				throw std::runtime_error (errMsg_ + " (Bad signature)");
			if (bmpHeader_.h1.bits_per_pixel != 1)
				throw std::runtime_error (errMsg_ + " (Incorrect bit depth)");
			if (bmpHeader_.h0.file_size - bmpHeader_.h0.bitmap_offset <= 0)
				throw std::runtime_error (errMsg_ + " (Incorrect bitmap size)");			
			detail::bmp_bitmap bmpData_ ({{
				bmpHeader_.h1.width, 
				bmpHeader_.h1.height}}, 0);
			auto &container_ = bmpData_.container ();
			auto const totalSize_ = sizeof (container_ [0]) * container_.size ();
			bmpFile_.seekg (bmpHeader_.h0.bitmap_offset, std::ios::beg);
			bmpFile_.read (reinterpret_cast<char *> (
				&container_ [0]), totalSize_);
			if (bmpFile_.gcount () != totalSize_)
				throw std::runtime_error (errMsg_ + " (Currupted data)");
			return bmpData_;		
		}
	
	}
}