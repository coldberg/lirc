#ifndef __TEXTIO_LINE_FILE_H__
#define __TEXTIO_LINE_FILE_H__

#include <string>
#include <vector>

namespace textio {
	template <typename CharType, typename EncodingHelperType, template <typename> class LineParser>
		struct line_file {
			typedef EncodingHelperType								encoding_helper_type;
			typedef LineParser<CharType>							line_parser_type;
			typedef typename LineParser<CharType>::line_entry_type	line_entry_type; 
			typedef typename LineParser<CharType>::char_type		char_type;
			typedef std::vector<line_entry_type>					container_type;
			static unsigned const char_type_size					= LineParser<CharType>::char_type_size;

			line_file (std::string const &path);

			line_entry_type &operator [] (unsigned index) ;
			line_entry_type const &operator [] (unsigned index) const ;
			
			container_type &items ();
			container_type const &items () const;
				
			std::uint_fast64_t size () const;
		private:
			container_type _list_items;

		};

}

#include "textio_line_file.hpp"

#endif