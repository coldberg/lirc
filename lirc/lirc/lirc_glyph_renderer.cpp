#include "../clifw/clifw_string_utils.h"
#include "lirc.h"
#include "lirc_glyph_renderer.h"
#include "lirc_exceptions.h"
#include "lirc_font_generator.hpp"
#include "lirc_reference_index.hpp"
#include "../bitmapio/bitmapio_backed_bitmap.hpp"
#include "../bitmapio/bitmapio_msbmp.hpp"
#include <algorithm>


using namespace lirc;

glyph_renderer::glyph_renderer (lirc::app &lirc_app) :
	lirc_app_ (lirc_app),
	auto_crop_ (!lirc_app.option ("no-auto-crop").present ())	
{
	FT_Error _error = 
		FT_Init_FreeType (&library_);

	if (_error) throw freetype_exception (
		"Error while initializing freetype '%d'", 
		_error);
		
	for (auto const &_fid : lirc_app.get_working_font_set ())
		get_face (_fid);	
}

glyph_renderer::~glyph_renderer () {
	std::for_each (face_map_.begin (), face_map_.end (), [&] (face_map_type::value_type &v) {
		std::for_each (v.second.begin (), v.second.end (), [&] (face_map_type::value_type::second_type::value_type &v) {
			FT_Done_Face (v.second);
		});
	});
	FT_Done_FreeType (library_);
}

FT_Face &glyph_renderer::get_face (font_id_type const &_fid, FT_Long const &_faceidx) {
	try {
		return face_map_.at (_fid).at (_faceidx);
	}
	catch (std::out_of_range const &) {
		auto &&pFontRef_ = get_font (_fid);
		auto &&pFaceRef_ = face_map_ [_fid][_faceidx];
		
		auto iError_ = FT_New_Face (library_, 
			pFontRef_.path ().c_str (), _faceidx, &pFaceRef_);

		lirc_app_.log ().info ("Loading and initializing '%s:%d'@%lf ...\n", 
			pFontRef_.name ().c_str (), _faceidx, pFontRef_.size ());

		if (iError_) throw freetype_exception (
			"Error 0x%x while loading face '%s:%d'", 
			iError_, pFontRef_.path ().c_str (), _faceidx);

		FT_Size_RequestRec sSizeReq_;
		memset (&sSizeReq_, 0, sizeof (sSizeReq_));		
		sSizeReq_.type = FT_SIZE_REQUEST_TYPE_NOMINAL;
		sSizeReq_.height = static_cast<FT_UInt> (round (pFontRef_.size () * 64.0));
		sSizeReq_.width = 0 ;
		sSizeReq_.horiResolution = 96;
		sSizeReq_.vertResolution = 96;
		
		iError_ = FT_Request_Size (pFaceRef_, &sSizeReq_);
		if (iError_) throw freetype_exception (
			"Error 0x%x while setting face '%s' size %d ", 
			iError_, pFontRef_.path ().c_str (), pFontRef_.size ());
				
		return pFaceRef_;
	}
}

glyph_renderer &lirc::glyph_renderer::dump_glyphs (std::string const & _outdir, font_precursor const & _font_prec) {
	std::tr2::sys::create_directory (std::tr2::sys::path (_outdir));
	
	auto _gid = _font_prec.first_glyph ();
	for (auto const &g: _font_prec.glyphs ()) {
		
		glyph_bitmap const &_gbmap = g.is_bitmap () ? 
			get_glyph_bitmap (g.bitmap (), g.baseline ()) : 
			get_glyph_bitmap (g.font_id (), g.char_id ());			
			
		std::string _file_path = clifw::helper::sprintf_format_string (
			g.is_bitmap () ? "%s/0x%04X-bitmap.bmp" : "%s/0x%04X-0x%04X.bmp", 
			_outdir.c_str (), _gid++, g.char_id ());
        if ((_gbmap.height () * _gbmap.width ()) < 1)
            continue;
		bitmapio::write_bmp (_file_path, _gbmap);
		lirc_app_.log ().info ("Writting glyph %30s (size: %4d x %4d [asc.: %4d]) ...\n", 
			_file_path.c_str (), 
			_gbmap.width (), 
			_gbmap.height (), 
			-_gbmap.rect ().top ());

	}
	lirc_app_.log ().info ("\n");
	return *this;
}

glyph_bitmap const &lirc::glyph_renderer::get_glyph_bitmap (font_id_type const &fid_, char_type const &ucGlyph_) {    
    //lirc_app_.log ()
    //    .set_attr (0x4f)
    //    .error ("Using font %d\n", fid_)
    //    .set_attr ();
        
	try {
		return glyph_cache_ [fid_].at (ucGlyph_);
	}
	catch (std::exception const &) {		
		auto refFace_ = std::ref (get_face (fid_));
		auto gIndex_ = FT_Get_Char_Index (refFace_, (FT_ULong)ucGlyph_);

		if (!gIndex_) {
			auto pLineRef_ = reference_index<char_type>::get ()
				.find_reference_string (ucGlyph_);

			if (refFace_.get ()->num_faces < 2) {
				throw freetype_exception ("Glyph 0x%04X not found in font '%s', referenced in:\n%s", 
					ucGlyph_, get_font (fid_).name ().c_str (), pLineRef_.c_str ());
			}

			for (auto idx_ = FT_Long (1); idx_ < refFace_.get ()->num_faces && !gIndex_; ++idx_) {
				refFace_ = std::ref (get_face (fid_, idx_));
				gIndex_ = FT_Get_Char_Index (refFace_, (FT_ULong)ucGlyph_);
			}

			if (!gIndex_) {
				throw freetype_exception ("Glyph 0x%04X not found in font '%s' or any of it's faces, referenced in:\n%s", 
					ucGlyph_, get_font (fid_).name ().c_str (), pLineRef_.c_str ());
			}
		}

		FT_Error _error = FT_Load_Glyph (
			refFace_.get (), gIndex_, 
			FT_LOAD_DEFAULT|FT_LOAD_TARGET_MONO|
			FT_LOAD_RENDER|FT_LOAD_CROP_BITMAP);

		if (_error) {
			throw freetype_exception ("Error 0x%x while loading glyph", _error);
		}

		auto const &ftFace_ = *refFace_.get () ;
		auto const &ftGlyph_ = *refFace_.get ()->glyph;
		auto const &ftBitmap_ = refFace_.get ()->glyph->bitmap;
		
		auto const isEmpty_ = !(ftBitmap_.buffer && ftBitmap_.width) ;
		auto const wpad_ = std::int32_t (isEmpty_ ? ftGlyph_.advance.x >> 6 : 0) ;
        
        auto const &fref_ = lirc_app_.get_font (fid_);
        auto const gbShift_ = fref_.baseline_shift_glyph (ucGlyph_);
        auto const gPad_ = fref_.local_padding (ucGlyph_);
        

		auto glyphProxy_ = bitmapio::proxy_bitmap<bitmapio::proxy_container<std::uint8_t>> (
			{ftBitmap_.buffer, ftBitmap_.buffer + ftBitmap_.pitch * ftBitmap_.rows}, {
                /*point */{
                    ftGlyph_.bitmap_left,
                    gbShift_ - ftGlyph_.bitmap_top
                }, 
                /*point */{
				    ftGlyph_.bitmap_left + ftBitmap_.width + wpad_,
				    gbShift_ - ftGlyph_.bitmap_top + ftBitmap_.rows
                }
            }, ftBitmap_.pitch);

        auto bbox_ = bitmapio::bounding_box (glyphProxy_);

        auto blsnap_ = int (lirc_app_.option ("baseline-auto-ajust").value ());

        auto snaph_ = 0;
        if (blsnap_ > 0 && std::abs (bbox_.bottom ()) <= blsnap_) {
            glyphProxy_.adjust_origin ({0, -bbox_.bottom ()});
            bbox_.adjust_origin ({0, -bbox_.bottom ()});
        }

        auto rect_ = bitmapio::make_rect (
            glyphProxy_.rect ().top (), 
            glyphProxy_.rect ().right () + gPad_.right (), 
            glyphProxy_.rect ().bottom (),
            glyphProxy_.rect ().left () - gPad_.left ()); 

        auto glyphResized_ = glyph_bitmap (rect_, 0);
        bitmapio::superimpose (glyphResized_, 
            !auto_crop_ || isEmpty_ ? 
                glyph_bitmap (glyphProxy_) : 
			    bitmapio::crop<glyph_bitmap> (glyphProxy_, bbox_));

		glyph_cache_ [fid_].emplace (ucGlyph_, glyphResized_);				
		return glyph_cache_ [fid_].at (ucGlyph_);
	}
}

glyph_bitmap const &lirc::glyph_renderer::get_glyph_bitmap (std::string const &path, std::int_fast32_t  const &baseline) {
	try {
		return glyph_bitmap_cache_.at (path);
	}
	catch (std::exception &) {
		auto gbmap_= bitmapio::read_bmp<glyph_bitmap> (path);
		gbmap_.set_origin ({0, -baseline});
		glyph_bitmap_cache_.insert (std::make_pair (path, gbmap_));
		return glyph_bitmap_cache_.at (path);
	}
}

font_reference const &glyph_renderer::get_font (font_id_type const &_fid) const {
	return lirc_app_.get_font (_fid);
}

glyph_bitmap const &glyph_renderer::get_glyph_bitmap (glyph_reference const &_gref) {
	if (_gref.is_bitmap ())
		return get_glyph_bitmap (_gref.bitmap(), _gref.baseline ());
	return get_glyph_bitmap (_gref.font_id (), _gref.char_id ());
}

glyph_renderer &glyph_renderer::generate_font (std::string const &inPath_, 
	font_precursor const &inPrec_, std::uint_fast32_t const &inFontType_) 
{
	switch (inFontType_) {	
		case 0x01: return font::generate<font::type_f01> (
			inPath_, inPrec_, lirc_app_, *this), *this;
		case 0x08: return font::generate<font::type_f08> (
			inPath_, inPrec_, lirc_app_, *this), *this;
		case 0x10: return font::generate<font::type_ftd> (
			inPath_, inPrec_, lirc_app_, *this), *this;
	}
	throw bad_font_op_exception (
		"Can't generate font of type %d", inFontType_);
	return *this;
}
