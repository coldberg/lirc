#ifndef __BITMAPIO_MSBMP_HPP__
#define __BITMAPIO_MSBMP_HPP__

#include "bitmapio_backed_bitmap.hpp"

namespace bitmapio {
	namespace detail {	
		static auto const bmp_bitmap_flags =
			bitmap_flags::invert_scanline_order|
			bitmap_flags::big_endian|
			bitmap_flags::msb_first|
			bitmap_flags::align;

		typedef backed_bitmap<std::uint32_t, bmp_bitmap_flags> bmp_bitmap; 

		void write_bmp (std::string const &outFile_, bmp_bitmap const &inBitmap_);
		bmp_bitmap read_bmp (std::string const &inFile_);
	}

	template <typename BitmapType>		
		inline void write_bmp (std::string const &outFile_, BitmapType const &inBitmap_) {
			detail::bmp_bitmap bmpData_ (inBitmap_);
			detail::write_bmp (outFile_, bmpData_);
		}

	template <typename OutBitmapType = detail::bmp_bitmap>
		inline OutBitmapType read_bmp (std::string const &bmpPath_) {
			return detail::read_bmp (bmpPath_);
		}
	
}


#endif