#ifndef __TEXTIO_LINE_FILE_HPP__
#define __TEXTIO_LINE_FILE_HPP__

#include "../clifw/clifw_validator.h"
#include "textio_exceptions.h"
#include <cstdint>
#include <cstdio>
#include <fstream>
#include <filesystem>

namespace textio {

	template <typename CharType, typename EncodingHelperType, template <typename> class LineParser>
		line_file<CharType, EncodingHelperType, LineParser>::line_file (std::string const &filename) {
			std::string _path_string = filename;//std::tr2::sys::path (filename);
			::clifw::validator::file_exists () (filename, true);

			std::ifstream _in_file (filename.c_str (), std::ios::binary);
			std::vector<encoding_helper_type::byte_type> _buffer (2048);
			line_parser_type::line_type _line;
			int _line_counter = 0;
			EncodingHelperType _encoding_helper;

            bool _was_brk = false;
            bool _is_brk = false;

			for (;;) try {
				_in_file.read (reinterpret_cast<char *> (&_buffer [0]), _buffer.size ());
				std::int64_t _size = _in_file.gcount ();
				encoding_helper_type::idword_type _char = 0; 
				int _bits = 0 ;
								
				for (std::int_fast32_t  i = 0; i < _size; ++i) {
					
					if (!_encoding_helper.decode_char (_buffer [i], _char, _bits)) 
						continue;					

					if (_bits > char_type_size)
						throw char_overflow_exception ("Utf-8 char too big, must be <= 16bits");

					char_type const _masked_char = _char & ((1 << char_type_size) - 1);

					_was_brk = _is_brk;
					_is_brk = line_parser_type::is_break (_masked_char);                    
					if (!_is_brk) {
						if (_was_brk) {
							++_line_counter;
							line_entry_type _line_entry;
							if (line_parser_type::parse_line (_line, _line_entry, std::make_tuple (_path_string, _line_counter))) { 
								_list_items.push_back (_line_entry);											
                            }
							_line.clear ();
						}
						_line.push_back (_masked_char);
					}				
				}
				if (_size < static_cast<decltype(_size)> (_buffer.size ())) {
					if (_line.size () < 1)
						break;					
					++_line_counter;
					line_entry_type _line_entry;
					if (line_parser_type::parse_line (_line, _line_entry, std::make_tuple (_path_string, _line_counter)))
						_list_items.push_back (_line_entry);				
					
					break;
				}
			}
			catch (clifw::exception const &e) {
				std::string msg = clifw::helper::sprintf_format_string ("%s(%d): %s", filename.c_str (), _line_counter, e.reason ().c_str ());
				throw clifw::exception (msg, e.code (), e.type ());
			}

		}

		
	template <typename CharType, typename EncodingHelperType, template <typename> class LineParser>
		typename LineParser<CharType>::line_entry_type &line_file<CharType, EncodingHelperType, LineParser>::operator [] (unsigned int index) {		
			return _list_items.at (index);
		}
		
	template <typename CharType, typename EncodingHelperType, template <typename> class LineParser>
		typename LineParser<CharType>::line_entry_type const &line_file<CharType, EncodingHelperType, LineParser>::operator [] (unsigned int index) const {		
			return _list_items.at (index);
		}

	template <typename CharType, typename EncodingHelperType, template <typename> class LineParser>
		std::uint_fast64_t line_file<CharType, EncodingHelperType, LineParser>::size () const {
			return _list_items.size () ;
		}

	template <typename CharType, typename EncodingHelperType, template <typename> class LineParser>
		typename line_file<CharType, EncodingHelperType, LineParser>::container_type &
			line_file<CharType, EncodingHelperType, LineParser>::items () 
		{
			return _list_items;	
		}
	template <typename CharType, typename EncodingHelperType, template <typename> class LineParser>
		typename line_file<CharType, EncodingHelperType, LineParser>::container_type const &
			line_file<CharType, EncodingHelperType, LineParser>::items () const
		{
			return _list_items;	
		}
}

#endif