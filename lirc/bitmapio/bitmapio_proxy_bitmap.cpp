#ifndef __BITMAPIO_PROXY_BITMAP_HPP__
#define __BITMAPIO_PROXY_BITMAP_HPP__

#include <type_traits>
#include <vector>

#include "bitmapio_proxy_bitmap.h"
#include "../lirc/lirc_endian_helper.hpp"

namespace bitmapio {
	namespace detail {
		typedef std::pair<std::size_t, std::uint8_t> bitmapio_index_type;

		template <typename BitmapType>
			inline typename std::enable_if<is_inverted_scanline_order<BitmapType>::value, coord<std::int32_t>>::type
				proxy_bitmap_transform_coords (BitmapType const &bitmap_, 
					typename BitmapType::coord_type const &inCoord_)
			{
				return invert (bitmap_.rect (), inCoord_, invert_flags::y);
			}

		template <typename BitmapType>
			inline typename std::enable_if<is_normal_scanline_order<BitmapType>::value, coord<std::int32_t>>::type
				proxy_bitmap_transform_coords (BitmapType const &bitmap_, 
					typename BitmapType::coord_type const &inCoord_)
			{
				return inCoord_;
			}


		template <typename BitmapType>
			inline typename std::enable_if<is_aligned<BitmapType>::value, bitmapio_index_type>::type
				proxy_bitmap_index (BitmapType const &bitmap_, 
					typename BitmapType::coord_type const &inCoord_)
			{
				auto const word_size_in_bits_ = BitmapType::word_size_in_bits;
				auto const coord_ = proxy_bitmap_transform_coords (bitmap_, inCoord_) - bitmap_.rect ().origin () ;
				return bitmapio_index_type {
					bitmapio_index_type::first_type  (coord_.y () * bitmap_.pitch () + coord_.x () / word_size_in_bits_), 
					bitmapio_index_type::second_type (coord_.x () % word_size_in_bits_)
				};
			}

		template <typename BitmapType>
			inline typename std::enable_if<is_not_aligned<BitmapType>::value, bitmapio_index_type>::type
				proxy_bitmap_index (BitmapType const &bitmap_, 
					typename BitmapType::coord_type const &inCoord_)
			{
				auto const word_size_in_bits_ = BitmapType::word_size_in_bits;
				auto const coord_ = proxy_bitmap_transform_coords (bitmap_,	inCoord_) - bitmap_.rect ().origin () ;								
				auto const index_ = coord_.y () * bitmap_.width () + coord_.x ();
				return bitmapio_index_type {
					bitmapio_index_type::first_type  (index_ / word_size_in_bits_), 
					bitmapio_index_type::second_type (index_ % word_size_in_bits_)
				};								
				
			}

		template <typename BitmapType>
			inline typename std::enable_if<is_little_endian<BitmapType>::value, typename BitmapType::word_type>::type
				proxy_bitmap_endian_fix (BitmapType const &bitmap_,
					typename BitmapType::word_type const &word_)
			{								
				return lirc::helper::lit_endian<typename BitmapType::word_type>::fix (word_);
			}

		template <typename BitmapType>
			inline typename std::enable_if<is_big_endian<BitmapType>::value, typename BitmapType::word_type>::type
				proxy_bitmap_endian_fix (BitmapType const &bitmap_,
					typename BitmapType::word_type const &word_)
			{								
				return lirc::helper::big_endian<typename BitmapType::word_type>::fix (word_);
			}

		template <typename BitmapType, typename IndexType>
			inline typename std::enable_if<is_msb_first<BitmapType>::value, typename BitmapType::word_type>::type
				proxy_bitmap_mask (
					BitmapType const &bitmap_,
					IndexType &&index_)
			{
				auto const word_size_in_bits_ = BitmapType::word_size_in_bits;
				return proxy_bitmap_endian_fix (bitmap_, 1 << (word_size_in_bits_ - std::get<1> (index_) - 1));
			}

		template <typename BitmapType,  typename IndexType>
			inline typename std::enable_if<is_lsb_first<BitmapType>::value, typename BitmapType::word_type>::type
				proxy_bitmap_mask (
					BitmapType const &bitmap_,
					IndexType &&index_)
			{
				return proxy_bitmap_endian_fix (bitmap_, 1 << std::get<1> (index_));
			}

	}
	
	template <typename ContainerType, std::uint32_t Flags>
		proxy_bitmap<ContainerType, Flags>::proxy_bitmap (
			container_type	const &inCont_, 
			rect_type		const &inRect_,
			std::size_t		const &inPitch_) :
			rect_			(inRect_),
			pitch_			(bitmap_pitch (inRect_, inPitch_)),
			size_			(bitmap_size (inRect_, pitch_)),
			container_		(inCont_)
		{}


	template <typename ContainerType, std::uint32_t Flags>
		typename proxy_bitmap<ContainerType, Flags>::value_type
			proxy_bitmap<ContainerType, Flags>::get (coord_type const &coord_) const 
		{			
			if (!contains (rect (), coord_))
				return false;
			auto const index_ = detail::proxy_bitmap_index (*this, coord_);
			auto const mask_ = detail::proxy_bitmap_mask (*this, index_);			
			auto const &load_ = container_ [std::get<0> (index_)] ;
			return !!(load_ & mask_);
		}

	template <typename ContainerType, std::uint32_t Flags>
		proxy_bitmap<ContainerType, Flags> &proxy_bitmap<ContainerType, Flags>::set (coord_type const &coord_, value_type const value_) {			
			if (!contains (rect (), coord_))
				return *this;
			auto const index_ = detail::proxy_bitmap_index (*this, coord_);
			auto const mask_ = detail::proxy_bitmap_mask (*this, index_);
			auto const load_ = container_ [std::get<0> (index_)] ;
			container_ [std::get<0> (index_)] = value_ ? load_ | mask_ : load_ & ~mask_;
			return *this;
		}

	template <typename ContainerType, std::uint32_t Flags>
		std::size_t proxy_bitmap<ContainerType, Flags>::bitmap_pitch (
			rect_type const &inRect_, std::size_t const &inPitch_) 
		{			
			auto &&ret_ = inPitch_ ? inPitch_ : detail::align_ceil (
				inRect_.width (), word_size_in_bits);
			return ret_;
		}
	
	template <typename ContainerType, std::uint32_t Flags>
		std::size_t proxy_bitmap<ContainerType, Flags>::bitmap_size (
			rect_type const &inRect_, std::size_t const &inPitch_) 
		{	
			auto &&ret_ = !detail::is_aligned<proxy_bitmap<ContainerType, Flags>>::value ?
				detail::align_ceil (inRect_.area (), word_size_in_bits):
				inRect_.height () * bitmap_pitch (inRect_, inPitch_);
			return ret_;
		}

	template <typename ContainerType, std::uint32_t Flags>
		typename proxy_bitmap<ContainerType, Flags>::container_type const &
			proxy_bitmap<ContainerType, Flags>::container () const 
		{
			return container_ ;
		}

	template <typename ContainerType, std::uint32_t Flags>
		typename proxy_bitmap<ContainerType, Flags>::container_type &
			proxy_bitmap<ContainerType, Flags>::container ()  
		{
			return container_ ;
		}

	template <typename ContainerType, std::uint32_t Flags>
		typename proxy_bitmap<ContainerType, Flags>::rect_type::value_type
			proxy_bitmap<ContainerType, Flags>::width () const 
		{
			return rect_.width ();
		}

	template <typename ContainerType, std::uint32_t Flags>
		typename proxy_bitmap<ContainerType, Flags>::rect_type::value_type
			proxy_bitmap<ContainerType, Flags>::height () const 
		{
			return rect_.height ();
		}

	template <typename ContainerType, std::uint32_t Flags>
		typename proxy_bitmap<ContainerType, Flags>::rect_type const &
			proxy_bitmap<ContainerType, Flags>::rect () const 
		{
			return rect_;
		}
	template <typename ContainerType, std::uint32_t Flags>
		typename proxy_bitmap<ContainerType, Flags>::size_type
			proxy_bitmap<ContainerType, Flags>::size () const 
		{
			return rect_.size ();
		}
	template <typename ContainerType, std::uint32_t Flags>
		typename proxy_bitmap<ContainerType, Flags>::coord_type const &
			proxy_bitmap<ContainerType, Flags>::origin () const 
		{
			return rect_.origin () ;
		}

	template <typename ContainerType, std::uint32_t Flags>
		std::size_t proxy_bitmap<ContainerType, Flags>::pitch () const {
			return pitch_;
		}

	template <typename ContainerType, std::uint32_t Flags>
		std::size_t proxy_bitmap<ContainerType, Flags>::length () const {
			return size_ ;
		}
	template <typename ContainerType, std::uint32_t Flags>
		typename proxy_bitmap<ContainerType, Flags>::rect_type const &
			proxy_bitmap<ContainerType, Flags>::adjust_origin (coord_type const &inDelta_)
		{
			return rect_.adjust_origin (inDelta_);
		}

	template <typename ContainerType, std::uint32_t Flags>
		typename proxy_bitmap<ContainerType, Flags>::rect_type const &
			proxy_bitmap<ContainerType, Flags>::set_origin (coord_type const &inOrigin_) 
		{			
			return rect_.set_origin (inOrigin_);
		}


	//////////////////////////////
	/// EXPLICIT INSTANTATIONS ///
	//////////////////////////////
	
	template struct proxy_bitmap<proxy_container<std::int8_t>,   0x0>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0x0>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0x0>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0x0>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0x0>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0x0>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0x0>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0x0>;

	template struct proxy_bitmap<proxy_container<std::int8_t>,   0x1>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0x1>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0x1>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0x1>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0x1>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0x1>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0x1>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0x1>;

	template struct proxy_bitmap<proxy_container<std::int8_t>,   0x2>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0x2>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0x2>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0x2>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0x2>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0x2>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0x2>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0x2>;

	template struct proxy_bitmap<proxy_container<std::int8_t>,   0x3>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0x3>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0x3>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0x3>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0x3>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0x3>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0x3>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0x3>;

	template struct proxy_bitmap<proxy_container<std::int8_t>,   0x4>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0x4>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0x4>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0x4>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0x4>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0x4>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0x4>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0x4>;

	template struct proxy_bitmap<proxy_container<std::int8_t>,   0x5>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0x5>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0x5>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0x5>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0x5>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0x5>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0x5>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0x5>;

	template struct proxy_bitmap<proxy_container<std::int8_t>,   0x6>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0x6>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0x6>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0x6>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0x6>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0x6>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0x6>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0x6>;

	template struct proxy_bitmap<proxy_container<std::int8_t>,   0x7>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0x7>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0x7>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0x7>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0x7>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0x7>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0x7>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0x7>;

	template struct proxy_bitmap<proxy_container<std::int8_t>,   0x8>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0x8>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0x8>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0x8>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0x8>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0x8>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0x8>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0x8>;

	template struct proxy_bitmap<proxy_container<std::int8_t>,   0x9>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0x9>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0x9>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0x9>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0x9>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0x9>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0x9>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0x9>;

	template struct proxy_bitmap<proxy_container<std::int8_t>,   0xA>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0xA>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0xA>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0xA>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0xA>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0xA>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0xA>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0xA>;

	template struct proxy_bitmap<proxy_container<std::int8_t>,   0xB>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0xB>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0xB>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0xB>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0xB>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0xB>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0xB>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0xB>;

	template struct proxy_bitmap<proxy_container<std::int8_t>,   0xC>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0xC>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0xC>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0xC>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0xC>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0xC>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0xC>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0xC>;

	template struct proxy_bitmap<proxy_container<std::int8_t>,   0xD>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0xD>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0xD>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0xD>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0xD>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0xD>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0xD>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0xD>;

	template struct proxy_bitmap<proxy_container<std::int8_t>,   0xE>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0xE>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0xE>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0xE>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0xE>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0xE>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0xE>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0xE>;

	template struct proxy_bitmap<proxy_container<std::int8_t>,   0xF>;
	template struct proxy_bitmap<proxy_container<std::uint8_t>,  0xF>;
	template struct proxy_bitmap<proxy_container<std::int16_t>,  0xF>;
	template struct proxy_bitmap<proxy_container<std::uint16_t>, 0xF>;
	template struct proxy_bitmap<proxy_container<std::int32_t>,  0xF>;
	template struct proxy_bitmap<proxy_container<std::uint32_t>, 0xF>;
	template struct proxy_bitmap<proxy_container<std::int64_t>,  0xF>;
	template struct proxy_bitmap<proxy_container<std::uint64_t>, 0xF>;


	template struct proxy_bitmap<std::vector<std::int8_t>,   0x0>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0x0>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0x0>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0x0>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0x0>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0x0>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0x0>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0x0>;
								 
	template struct proxy_bitmap<std::vector<std::int8_t>,   0x1>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0x1>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0x1>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0x1>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0x1>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0x1>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0x1>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0x1>;
								 
	template struct proxy_bitmap<std::vector<std::int8_t>,   0x2>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0x2>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0x2>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0x2>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0x2>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0x2>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0x2>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0x2>;
								 
	template struct proxy_bitmap<std::vector<std::int8_t>,   0x3>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0x3>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0x3>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0x3>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0x3>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0x3>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0x3>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0x3>;
								 
	template struct proxy_bitmap<std::vector<std::int8_t>,   0x4>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0x4>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0x4>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0x4>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0x4>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0x4>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0x4>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0x4>;
								 
	template struct proxy_bitmap<std::vector<std::int8_t>,   0x5>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0x5>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0x5>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0x5>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0x5>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0x5>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0x5>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0x5>;
								 
	template struct proxy_bitmap<std::vector<std::int8_t>,   0x6>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0x6>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0x6>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0x6>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0x6>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0x6>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0x6>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0x6>;
								 
	template struct proxy_bitmap<std::vector<std::int8_t>,   0x7>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0x7>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0x7>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0x7>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0x7>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0x7>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0x7>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0x7>;
								 
	template struct proxy_bitmap<std::vector<std::int8_t>,   0x8>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0x8>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0x8>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0x8>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0x8>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0x8>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0x8>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0x8>;
								 
	template struct proxy_bitmap<std::vector<std::int8_t>,   0x9>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0x9>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0x9>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0x9>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0x9>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0x9>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0x9>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0x9>;
								 
	template struct proxy_bitmap<std::vector<std::int8_t>,   0xA>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0xA>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0xA>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0xA>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0xA>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0xA>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0xA>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0xA>;
								 
	template struct proxy_bitmap<std::vector<std::int8_t>,   0xB>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0xB>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0xB>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0xB>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0xB>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0xB>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0xB>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0xB>;
								 
	template struct proxy_bitmap<std::vector<std::int8_t>,   0xC>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0xC>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0xC>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0xC>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0xC>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0xC>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0xC>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0xC>;
								 
	template struct proxy_bitmap<std::vector<std::int8_t>,   0xD>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0xD>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0xD>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0xD>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0xD>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0xD>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0xD>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0xD>;
								 
	template struct proxy_bitmap<std::vector<std::int8_t>,   0xE>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0xE>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0xE>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0xE>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0xE>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0xE>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0xE>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0xE>;
								 
	template struct proxy_bitmap<std::vector<std::int8_t>,   0xF>;
	template struct proxy_bitmap<std::vector<std::uint8_t>,  0xF>;
	template struct proxy_bitmap<std::vector<std::int16_t>,  0xF>;
	template struct proxy_bitmap<std::vector<std::uint16_t>, 0xF>;
	template struct proxy_bitmap<std::vector<std::int32_t>,  0xF>;
	template struct proxy_bitmap<std::vector<std::uint32_t>, 0xF>;
	template struct proxy_bitmap<std::vector<std::int64_t>,  0xF>;
	template struct proxy_bitmap<std::vector<std::uint64_t>, 0xF>;
}

#endif