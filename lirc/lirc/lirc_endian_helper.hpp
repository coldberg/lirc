#ifndef __LIRC_ENDIAN_HELPER_H__
#define __LIRC_ENDIAN_HELPER_H__

#include <type_traits>
#include <cstdint>

namespace lirc {
	namespace helper {


		namespace endian {
			
			inline std::uint8_t swap (std::uint8_t _) {
				return _;
			}

			inline std::uint16_t swap (std::uint16_t _) {
				return (_ >> 8) | (_ << 8);
			}

			inline std::uint32_t swap (std::uint32_t _) {
				return  
					(static_cast<std::uint32_t>(swap (static_cast<std::uint16_t> (_ >>  0))) << 16)|
					(static_cast<std::uint32_t>(swap (static_cast<std::uint16_t> (_ >> 16))) <<  0);					
			}

			inline std::uint64_t swap (std::uint64_t _) {
				return 
					(static_cast<std::uint64_t>(swap (static_cast<std::uint32_t> (_ >>  0))) << 32)|
					(static_cast<std::uint64_t>(swap (static_cast<std::uint32_t> (_ >> 32))) <<  0);
			}

			inline std::int8_t swap (std::int8_t _) { return static_cast<std::int8_t>(swap (static_cast<std::uint8_t> (_))); }
			inline std::int16_t swap (std::int16_t _) { return static_cast<std::int16_t>(swap (static_cast<std::uint16_t> (_))); }
			inline std::int32_t swap (std::int32_t _) { return static_cast<std::int32_t>(swap (static_cast<std::uint32_t> (_))); }
			inline std::int64_t swap (std::int64_t _) { return static_cast<std::int64_t>(swap (static_cast<std::uint64_t> (_))); }
		}

		template <typename T>
			struct lit_endian {
				static inline T fix (T _) {
					return _;
				}			
			};

		template <typename T>
			struct big_endian {
				static inline T fix (T _) {
					return endian::swap (_);
				}			
			};



	}
}

#endif