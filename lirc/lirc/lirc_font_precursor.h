#ifndef __LIRC_FONT_PRECURSOR_H__
#define __LIRC_FONT_PRECURSOR_H__

#include "../bitmapio/bitmapio_rect.h"
#include "lirc_font_reference.h"
#include "lirc_glyph_reference.h"

#include <vector>

namespace lirc {
	
	struct font_precursor {	
		typedef font_reference::char_type char_type;		
		typedef glyph_reference::charset_type charset_type;
		typedef glyph_reference::font_id_type font_id_type;
		typedef glyph_reference::font_size_type font_size_type;
		typedef font_reference::glyph_map_type glyph_map_type;
		typedef std::vector<glyph_reference> glyph_collection_type;
		typedef font_reference::char_map_type char_map_type;        
        typedef bitmapio::rect<std::int_fast32_t> rect_type;
		
		glyph_collection_type const &glyphs () const;
		glyph_collection_type &glyphs () ;
		char_type const &first_glyph () const;
		char_type const &last_glyph () const;
		char_type const &default_glyph () const;
		rect_type const &padding () const;
        font_reference const &fref () const;

		font_precursor (font_id_type const &fid_, font_reference const &fref_);

	private:
		glyph_collection_type  _glyphs;
        font_reference const & _fref;
	};
}

#endif