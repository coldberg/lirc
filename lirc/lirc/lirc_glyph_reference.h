#ifndef __LIRC_GLYPH_REFERENCE_H__
#define __LIRC_GLYPH_REFERENCE_H__

#include <set>

namespace lirc {

	struct glyph_reference {
		typedef std::uint32_t				font_id_type;
		typedef wchar_t						char_type;
		typedef std::set<char_type>			charset_type;
		typedef std::set<font_id_type>		font_set_type;
		typedef double						font_size_type;

		glyph_reference (char_type const &_char, font_id_type const &_font_id, font_size_type const &prefered_size) :
			_char (_char),
			_font_id (_font_id),
			_path (""),
			_prefered_size (prefered_size),
			_baseline (-1)
		{}

		glyph_reference (std::string const &_path, font_size_type const &prefered_size, std::int_fast32_t const &baseline = -1):
			_char ((char_type)-1),
			_font_id ((font_id_type)-1),			
			_path (_path),
			_prefered_size (prefered_size),
			_baseline (baseline < 0 ? static_cast<std::int_fast32_t> (round (_prefered_size * 0.68)) : baseline)
		{
			if (_baseline == 44)
				__debugbreak () ;
		}

		char_type const &char_id () const {
			return _char;
		}

		font_id_type const &font_id () const {
			return _font_id;
		}

		bool is_bitmap () const {
			return _path != "" && _char == (char_type)-1 && _font_id == (font_id_type)-1;
		}

		std::string const &bitmap () const {
			return _path;
		}

		font_size_type const &size () const {
			return _prefered_size;
		}

		std::int_fast32_t  const &baseline () const {			
			return _baseline;
		}

	private:
		char_type		_char;
		font_id_type	_font_id;
		std::string		_path;
		font_size_type	_prefered_size;
		std::int_fast32_t 			_baseline;
	};
}

#endif