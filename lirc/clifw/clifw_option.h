#ifndef __CLIFW_OPTION_H__
#define __CLIFW_OPTION_H__

namespace clifw {
	template <typename UserApp>
		struct app;

	template <typename UserApp>
		struct option_type {	
			enum : unsigned int {
				BOOLEAN_SWITCH,
				OPTIONAL_VALUE,
				REQUIRED_VALUE
			};

			typedef typename app<UserApp>::callback_type callback_type ;
			typedef typename app<UserApp>::argument_type argument_type ;
			typedef typename app<UserApp>::output_type output_type ;
			typedef std::function<bool (auto_var<std::string> const &value, bool throw_exception)> validator_type;

			option_type (app<UserApp> &app, std::string const &name, std::string const &alias);
			option_type &description (std::string const &);				
			option_type &flags (unsigned int);
			option_type &value (argument_type const &, bool = false);
			option_type &callback (callback_type const &);
			option_type &validator (validator_type const &);

			template <typename Class>
				option_type &callback (Class *, bool (Class::*)(
					typename Class::option_type &, 
					typename Class::output_type &));

			option_type &argument (int const &);

			std::string const &name () const;
			std::string const &alias () const;
			std::string const &description () const;				
			unsigned int const &flags () const;
			argument_type const &value () const;
			callback_type const &callback () const;
			int const &argument () const;
			bool const &present () const;

			bool check () const ;

			int takes_value () const;
			int needs_value () const;

			app<UserApp> &parent () ;
			app<UserApp> const &parent () const;

			struct validator_none {
				bool operator () (auto_var<std::string> const &value, bool throw_exception) {
					return true;
				}
			};

		private:
			app<UserApp> &_app;
			std::string _name;
			std::string _alias;
			std::string _description;				
			unsigned int _flags;				
			argument_type _value;
			callback_type _callback;
			validator_type _validator;
			bool _present;
			int _argument;
		} ;

}

#include "clifw_option.hpp"

#endif