#include <algorithm>

#include "lirc.h"
#include "lirc_tilsv.h"
#include "lirc_spasv.h"
#include "lirc_translation_resource.h"
#include "lirc_utils.h"
#include "lirc_font_precursor.h"
#include "lirc_glyph_renderer.h"
#include "lirc_resource_generator.h"
#include "../bitmapio/bitmapio_rect.h"
#include "lirc_version.h"

lirc::app::app () :
	clifw::app<lirc::app> (*this)
{
	meta ("name",			"LIRC");
	meta ("description",	"LIfoda Resource Compiler");
	meta ("author",			"Lifoda");
	meta ("year",			"2014");
	meta ("version",		__lirc_version__);	
	meta ("build",			__lirc_build__  );	

	add_argument ("translation-project")
		.description ("Translation project file.")
		.flags (option_type::REQUIRED_VALUE)
		.validator (clifw::validator::file_not_empty ());
	add_argument ("output-directory")
		.description ("Directory to ouput all files to.")
		.flags (option_type::REQUIRED_VALUE);
		
	add_option ("font-padding", "T")
		.description ("Set the output font padding type, valid values are 1, 8 or 16.")
		.flags (option_type::REQUIRED_VALUE)
		.value (8, true)
		.validator (::clifw::validator::in_list ({1, 8, 16}));
	/*
	add_option ("font-override-space-width", "S")
		.description ("Set default size for fonts that don't specify a size.")
		.flags (option_type::REQUIRED_VALUE)
		.value (0, true)
		.validator (::clifw::validator::make_lambda_validator ([] (clifw::auto_var<std::string> const &inExp_){
			return int (inExp_) >= 2;
		}, "minimum space width must be atleast 2"));
		*/
	add_option ("font-minimum-width", "W")
		.description ("Set default size for fonts that don't specify a size.")
		.flags (option_type::REQUIRED_VALUE)
		.value (3, true)
		.validator (::clifw::validator::make_lambda_validator ([] (clifw::auto_var<std::string> const &inExp_){
			return int (inExp_) >= 3;
		}, "minimum width must be atleast 3"));

	add_option ("font-id-offset", "I")
		.description ("Set the font ID offset when generating resources")
		.flags (option_type::REQUIRED_VALUE)
		.value (0xA, true)
		.validator (::clifw::validator::integer_value ());	
	add_option ("empty-font-placeholder", "E")
		.description ("Sets the place holder for empty fonts in font list.")
		.flags (option_type::REQUIRED_VALUE)
		.value ("RESERVED", true);
	add_option ("font-list-name", "L")
		.description ("Sets name for fontlist file.")
		.flags (option_type::REQUIRED_VALUE)
		.value ("fonts.lst", true);		

	add_option ("generate-charmaps", "M")
		.description ("Generate .map files for each generated font.")
		.flags (option_type::BOOLEAN_SWITCH);
	add_option ("generate-bitmaps", "B")
		.description ("Generate bitmap files for each font glyph.")
		.flags (option_type::BOOLEAN_SWITCH);
	add_option ("no-auto-crop", "C")
		.description ("Don't automatically crop bitmap. Speeds up rendering a bit but leaves extra padding for some glyphs sometimes.")
		.flags (option_type::BOOLEAN_SWITCH);

	add_option ("baseline-auto-ajust", "J")
		.description ("Attempt to automatically adjust baseline, if glyph sticks out more then specified value of pixels, 0 - disable feature")
		.flags (option_type::REQUIRED_VALUE)
		.value (0, true)
		.validator (::clifw::validator::integer_value ());	

	add_option ("no-global-ascent", "A")
		.description ("Set ascent same as height.")
		.flags (option_type::BOOLEAN_SWITCH);
	add_option ("remap-empty-entries", "R")
		.description ("Exclude empty entries from font list and remap the rest.")
		.flags (option_type::BOOLEAN_SWITCH);    

	#if defined (_WIN32) || defined (_WIN64)
	add_option ("skip-system-fonts", "F")
		.description ("Don't look for fonts in system font directory.")
		.flags (option_type::BOOLEAN_SWITCH);		
	#endif
	
}

lirc::app::~app () {
#ifdef __DEBUG__
 	log ().pause () ;	
#endif
}

lirc::font_reference const &lirc::app::get_font (font_id_type const &_fid) const try {
	return _font_reference_set.at (_fid);
}
catch (std::exception const &) {
	throw lirc::bad_font_id_exception ("Font with the id [%4d] not loaded.", _fid);
}

lirc::font_reference &lirc::app::get_font (font_id_type const &_fid) try {
	return _font_reference_set.at (_fid);
}
catch (std::exception const &) {
	throw lirc::bad_font_id_exception ("Font with the id [%4d] not loaded.", _fid);
}


lirc::app::font_set_type const &lirc::app::get_working_font_set () const {
	return _working_font_set;
}

int lirc::app::main (argument_list_type const &args, output_type &out) {	
	auto const _path = std::string (args ["translation-project"]);
	auto _project_path = std::tr2::sys::path (_path);
	auto _base_path = _project_path.branch_path ().string ();
	if (_base_path != "")
		_base_path += "/";

	auto _items = 
		clifw::helper::file_type(_path) == ".tilsv" ?
		tilsv_file<char> (_project_path.string ()).items () :
		spasv_file<char> (_project_path.string ()).items () ;

	for (auto const &it: _items) {
		try {
			auto const &_cmd = it.at (0);
			if (_cmd == "add-font-list") {
				add_font_list (_base_path + it.at (1));
				continue;
			}

			if (_cmd == "add-font") {
				add_font (
					_base_path + it.at (1), 
					it.size () > 2 ? it.at (2) : "", 
					it.size () > 3 ? it.at (3) : "");
				continue;
			}

			if (_cmd == "add-native-font") {
				add_native_font (
					_base_path + it.at (1), 
					it.size () > 2 ? _base_path + it.at (2) : "");
				continue;
			}

			if (_cmd == "add-translation") {
				add_translation (_base_path + it.at (1));
				continue;
			}

			if (_cmd == "fixup-include-glyph-range") {
				fixup_include_glyph_range (
					it.at (1), it.at (2), it.at (3));
				continue;
			}

			if (_cmd == "fixup-include-glyphs") {
				fixup_include_glyphs (it.at (1), 
					{it.begin ()+2, it.end ()});
				continue;
			}

			if (_cmd == "fixup-override-glyph-range") {
				fixup_override_glyph_range (
					it.at (1), it.at (2), 
					it.at (3), it.at (4));
				continue;
			}
			if (_cmd == "fixup-override-glyphs") {
				fixup_override_glyphs (
					it.at (1), it.at (2), 
					{it.begin ()+3, it.end ()});
				continue;
			}
			if (_cmd == "fixup-override-glyph-bitmap") {
				fixup_override_glyph_bitmap (
					it.at (1), it.at (2), 
					_base_path + it.at (3), 
					it.size () > 4 ? it.at (4) : "-1");
				continue;
			}
			if (_cmd == "fixup-per-font-padding") {
				fixup_per_font_padding (it.at (1), 
					it.size () > 2 ? it.at (2) : "0",
					it.size () > 3 ? it.at (3) : "0",
					it.size () > 4 ? it.at (4) : "0",
					it.size () > 5 ? it.at (5) : "0");
				continue;
			}
            if (_cmd == "fixup-baseline-shift-glyphs") { 
                fixup_baseline_shift_glyphs (
                    it.at (1),  // font id
                    it.at (2),  // shift
                    {it.begin () + 3, it.end ()} // glyphs
                );
                continue;
            }
            if (_cmd == "fixup-per-glyph-padding") { 
                fixup_per_glyph_padding (
                    it.at (1),  // font id
                    it.at (2),  // pad left
                    it.at (3),  // pad right
                    {it.begin () + 4, it.end ()} // glyphs
                );
                continue;
            }

			throw lirc::bad_command_exception ("Bad command");
		}
		catch (clifw::exception const &e) {
			log ().set_attr (0x4F);
			log ().warning ("WANRING: Encountered error (%s) while processing command '%s', ignoring.\n", e.reason ().c_str (), it.at (0).c_str ());
			log ().set_attr ();
			continue;
		}
		catch (std::exception const &e) {
			log ().set_attr (0x4F);
			log ().warning ("WANRING: Encountered error (%s) while processing command '%s', ignoring.\n", e.what (), it.at (0).c_str ());
			log ().set_attr ();
			continue;
		}
	}

	log ().info ("Number of fonts to be generated : %d\n", _working_font_set.size ());
	log ().info ("Number of native fonts to be included : %d\n", _native_font_set.size ());
	for_each_font ([&] (font_reference const &_fref, font_id_type const &_fid) {		
		if (_fref.is_native ()) {
			log ().info ("Font [%4d*] '%s', number of glyphs %d\n", 
				_fid, _fref.path ().c_str (), _fref.charmap ().size ());
			return;
		}
		if (_fref.charset ().size () > 0 || _fref.patches ().size () > 0) {
			log ().info ("Font [%4d ] '%s'@%lf, number of glyphs %d, number of patched glyphs %d\n", 
				_fid, _fref.name ().c_str (), _fref.is_native () ? 0 : _fref.size (), _fref.charset ().size (), _fref.patches ().size ());		
			return;
		}
		log ().info ("Font [%4d ] Not used, skipping...\n", _fid);
		
	}, false);


	auto const _out_path = std::tr2::sys::path (std::string (option ("output-directory").value ()));
	if (!std::tr2::sys::exists (_out_path)) {
		std::tr2::sys::create_directory (_out_path);
		if (!std::tr2::sys::exists (_out_path)) {
			throw filesystem_exception ("Couldn't create directory '%s'", _out_path.string ().c_str ());
		}
	}
	std::string _str_out_path = _out_path.string () != "" ? _out_path.string () + "/" : "";

	{
		font_set_type _all_fonts = {
			_native_font_set.begin (), 
			_native_font_set.end ()};
		_all_fonts.insert (_working_font_set.begin (), _working_font_set.end ());
		font_id_type i = 0;
		font_id_type _foff = option ("font-id-offset").value ();
		for (auto const &_fid: _all_fonts) {			
			_font_map.insert (std::make_pair (_fid, _foff + (option ("remap-empty-entries").present () ? i : _fid)));
			++i;
		}
	}
	
	{
		log ().info ("Preloading faces...\n");
		lirc::glyph_renderer _renderer (*this);

		if (!option ("remap-empty-entries").present ()) {
			for_each_font ([&] (font_reference &_fref, font_id_type const &_fid) {
				_font_name_list.insert (std::make_pair (_fid, option ("empty-font-placeholder").value ()));
			}, false);
		}

		for_each_font ([&] (font_reference &_fref, font_id_type const &_fid) {
			auto const &_charmap = _fref.generate_charmap ();		
			if (option ("generate-charmaps").present ()) {
				auto const _path = std::tr2::sys::path (_fref.path ());
				auto const _path_str = _str_out_path + std::to_string (_fid) + ".map";
				log ().info ("Generating charmap for [%4d] '%s'@%lf (%s)...\n", _fid, _fref.name ().c_str (), _fref.size (), _path_str.c_str ());		
				std::ofstream _cmap (_path_str);
				for (auto const &_it: _charmap) {
					_cmap << clifw::helper::sprintf_format_string ("0x%04X,0x%04X", _it.first, _it.second) << "\n";
				}
			}
			font_precursor _fprec (_fid, _fref);
			if (option ("generate-bitmaps").present ()) {
				_renderer.dump_glyphs (_str_out_path + std::to_string (_fid), _fprec);
			}

			std::uint_fast32_t _type = option ("font-padding").value ();
			std::string _ext = ".ftd";
			switch (_type) {
				case 1:
					_ext = ".f01"; break;
				case 8:
					_ext = ".f08"; break;
				case 16:
				default:
					_ext = ".ftd"; break;
			}
			auto const _outf = _str_out_path + std::to_string (_fid) + _ext;
			log ().info ("Generating '%s' ...\n", _outf.c_str ()); 
			if (option ("remap-empty-entries").present ()) {
				_font_name_list.insert (std::make_pair (_fid, _outf));
			} else {
				_font_name_list [_fid] = _outf;
			}
			_renderer.generate_font (_outf, _fprec, _type);
		}, true);	

		for (auto const &_fid: _native_font_set) { 
			auto const &_nfref = get_font (_fid);
			std::string _destpath = _str_out_path + std::to_string (_fid) + std::tr2::sys::path (_nfref.path ()).extension () ;
			log ().info ("Copying font '%s' to '%s' ...\n", _nfref.path ().c_str (), _destpath.c_str ());
			if (option ("remap-empty-entries").present ()) {
				_font_name_list.insert (std::make_pair (_fid, _destpath));
			} else {
				_font_name_list [_fid] = _destpath ;
			}
			try {
				std::tr2::sys::copy_file (
					std::tr2::sys::path (_nfref.path ()),
					std::tr2::sys::path (_destpath),
					std::tr2::sys::copy_option::overwrite_if_exists);
			} catch (std::exception const &) {
				log ().set_attr (0x4F)
					.error ("Failed to copy file '%s' to '%s', ignoring...\n", 
						_nfref.path ().c_str (), _destpath.c_str ())
					.set_attr ();
			}
		}
	}
	{
		resource_generator _resgen (*this);

		if (_translation_resources.size () > 0) {			
			for (auto const &_tres : _translation_resources) {
				std::string _file = _str_out_path + _tres.name () + ".res";
				log ().info ("Generating '%s' ...\n", _file.c_str ());
				_resgen.write (_file, _tres);
			}
			std::string _file = _str_out_path + std::string (option ("font-list-name").value ());

			resource_generator::font_list_type _flist ;
			for (auto const &it : _font_name_list) {
				auto const _fname = std::tr2::sys::path (it.second).filename ();
				_flist.push_back (string_type (_fname.begin (), _fname.end ()));
			}
			log ().info ("Generating '%s' ...\n", _file.c_str ());
			_resgen.write (_file, _flist);
		}
		else {
			log ().info ("No translation resources defined, ommiting resource output.");
		}


	}		

	return 0;
}

lirc::app &lirc::app::add_font_list (std::string const &_path) {
	auto _font_list_path = std::tr2::sys::path (_path);
	auto _items = 
		clifw::helper::file_type (_path) == ".tilsv" ?
		tilsv_file<char> (_font_list_path.string ()).items () :
		spasv_file<char> (_font_list_path.string ()).items ();

	for (auto const &it : _items) {
		add_font (_font_list_path.branch_path ().string () + "/" + it.at (0), it.size () > 1 ? it.at (1) : "", it.size () > 2 ? it.at (2) : "");	
	}
	return *this;
}

lirc::app &lirc::app::add_font (std::string const &_path, std::string const &_size, std::string const &default_char) {
	font_size_type _font_size = 0;//option ("font-default-size").value () ;
	char_type _default_char = default_char != "" ? clifw::universal_cast<char_type> (default_char) : '-';

	if (clifw::validator::number_value () (_size, false)) {
		_font_size = clifw::universal_cast<font_size_type> (_size);
	}
	else {
		log ().set_attr (0x4F);
		log ().warning ("\tWARNING: No valid size specified for font '%s', using default (%d).\n", _path.c_str (), _font_size);
		log ().set_attr ();
	}

	_total_font_set.insert (font_id_type (_font_reference_set.size ()));
	_font_reference_set.push_back (
		lirc::app::font_reference_type (
			locate_font (_path), _font_size, "", _default_char));
	log ().info ("Adding font reference [%4d] '%s'@%lf ...\n",
		_font_reference_set.size () - 1,
		_font_reference_set.back ().path ().c_str (), 
		_font_reference_set.back ().size ());
	return *this;
}

lirc::app &lirc::app::add_native_font (std::string const &_path, std::string const &_cmap) {	
	clifw::validator::file_not_empty () (_path, true);
	_native_font_set.insert (font_id_type (_font_reference_set.size ()));
	_total_font_set.insert (font_id_type (_font_reference_set.size ()));
	_font_reference_set.push_back (
		lirc::app::font_reference_type (_path, "", _cmap));		
	log ().info ("Adding native font reference [%4d] '%s (%s)' ...\n",
		_font_reference_set.size () - 1,
		_font_reference_set.back ().path ().c_str (), 
		_cmap.c_str ());

	return *this;
}

std::string lirc::app::locate_font (std::string const &_path) const {
	
	if (clifw::validator::file_not_empty () (_path, false))
		return _path;
#if defined(_WIN32) || defined(_WIN64)
	if (!option ("skip-system-fonts").present ()) {
		auto _font_path = std::tr2::sys::path (_path);
		auto _font_alt_path = getenv ("WINDIR") + ("/Fonts/" + _font_path.filename ()) ;	
		if (clifw::validator::file_not_empty () (_font_alt_path, false))
			return _font_alt_path;
	}
#endif
	throw ::clifw::validator::file_not_empty::exception ("File '%s' doesn't exist or is empty", _path.c_str ());
	return "";
}

lirc::app &lirc::app::add_translation (std::string const &_path) {	
	auto _translation_path = std::tr2::sys::path (_path);
	log ().info ("Loading translation resource [%4d] '%s' ...\n", _translation_resources.size (), _path.c_str ());
	_translation_resources.push_back (lirc::translation_resource (_path));
	auto const &_tres = _translation_resources.back ();			// get the inserted resource reference
	auto const &_fset = _tres.font_set ();						// get it's font id set
	for (auto const &_fid : _fset) {							// iterate over all ids		
		if (get_font (_fid).is_native ())
			continue;		
		add_char (_fid, _tres.font_charset (_fid));
	}	

	return *this;
}

lirc::app &lirc::app::fixup_include_glyph_range (std::string const &_fid, std::string const &rx, std::string const &ry) {
	auto const _rx = clifw::universal_cast<char_type> (rx);
	auto const _ry = clifw::universal_cast<char_type> (ry);
	auto const _crange = lirc::utils::make_range (_rx, _ry);		
	auto const _msg = "Forcing character range 0x%04X - 0x%04X to font [%4d]...\n";
	auto const _lambda = [&] (font_id_type const &_font_id) {
		log ().info (_msg, _rx, _ry, _font_id);
		add_char (_font_id, {_crange.begin (), _crange.end ()});
	};

	if (_fid != "*") {				
		_lambda (clifw::universal_cast<font_id_type> (_fid));
		return *this;
	}
	for_each_font_id (_lambda, true);
	return *this;	
}

lirc::app &lirc::app::fixup_include_glyphs (std::string const &_fid, std::list<std::string> const &_specifc) {
	std::string _chars ;
	charset_type _cset ;

	for (auto const &_ch : _specifc) 
		_chars += (_ch + " ");
	
	auto const _msg = "Forcing characters %sto font [%4d]...\n";
	
	for (auto const &_cid : _specifc) 
		_cset.insert (clifw::universal_cast<char_type> (_cid));
	
	auto const _lambda = [&] (font_id_type const &_font_id) {
		log ().info (_msg, _chars.c_str (), _font_id);
		add_char (_font_id, _cset);
	};

	if (_fid != "*") {				
		_lambda (clifw::universal_cast<font_id_type> (_fid));
		return *this;
	}
	for_each_font_id (_lambda, true);
	return *this;	
}

lirc::app &lirc::app::fixup_override_glyph_range (std::string const &fid, std::string const &from_fid, std::string const &rx, std::string const &ry) {
	auto const _rx = clifw::universal_cast<char_type> (rx);
	auto const _ry = clifw::universal_cast<char_type> (ry);
	auto const _crange = lirc::utils::make_range (_rx, _ry, true);
	auto const _msg = "Injecting character range 0x%04X - 0x%04X from font [%4d] to font [%4d]...\n";
	auto const _from_font_id = clifw::universal_cast<font_id_type> (from_fid);
	auto const _lambda = [&] (font_id_type const &_font_id) {
		if (_font_id == _from_font_id)
			return;
		log ().info (_msg, _rx, _ry, _from_font_id, _font_id);
		inject_char (_font_id, _from_font_id, {_crange.begin (), _crange.end ()});		
	};
	if (fid != "*") {		
		font_id_type _font_id = clifw::universal_cast<font_id_type> (fid);		
		_lambda (_font_id);
		return *this;
	}
	for_each_font_id (_lambda, true);
	return *this;
}

lirc::app &lirc::app::fixup_override_glyphs (std::string const &_fid, std::string const &from_fid, std::list<std::string> const &_specifc) {
	std::string _chars ;
	charset_type _cset;
	font_id_type _from_font_id = clifw::universal_cast<font_id_type> (from_fid);

	for (auto const &_ch : _specifc) 
		_chars += (_ch + " ");	
	auto const _msg = "Injecting characters %sfrom font [%4d] to font [%4d]...\n";

	for (auto const &_cid: _specifc) 
		_cset.insert (clifw::universal_cast<wchar_t> (_cid));
	
	auto _lambda = [&] (font_id_type const &_fid) {
		if (_fid == _from_font_id)
			return;
		log ().info (_msg, _chars.c_str (), _from_font_id, _fid);
		inject_char (_fid, _from_font_id, _cset);
	};

	if (_fid != "*") {				
		_lambda (clifw::universal_cast<font_id_type> (_fid));
		return *this;
	}
	for_each_font_id (_lambda, true);
	return *this;	
}


lirc::app &lirc::app::fixup_override_glyph_bitmap (
	std::string const &_fid, 
	std::string const &_glyph, 
	std::string const &_bitmap,
	std::string const &_baseline) 
{
	clifw::validator::file_not_empty () (_bitmap, true);
	auto const _cid = clifw::universal_cast<char_type> (_glyph);
	auto const _msg = "Injecting glyph 0x%04X bitmap into font [%4d]...\n";

	auto const _lambda = [&] (font_id_type const &_fid) {
		inject_bitmap (_fid, _cid, _bitmap, clifw::universal_cast<std::int_fast32_t > (_baseline));
	};

	if (_fid != "*") {
		_lambda (clifw::universal_cast<font_id_type> (_fid));
		return *this;
	}
	
	for_each_font_id (_lambda, true);

	return *this;
}

lirc::app &lirc::app::fixup_per_font_padding (
	std::string const &_sfid,
	std::string const &_stop,
	std::string const &_sright,
	std::string const &_sbottom,
	std::string const &_sleft)
{	
	auto const _rect = bitmapio::make_rect (
		clifw::universal_cast<std::int_fast32_t> (_stop),
		clifw::universal_cast<std::int_fast32_t> (_sright),
		clifw::universal_cast<std::int_fast32_t> (_sbottom),
		clifw::universal_cast<std::int_fast32_t> (_sleft));
	auto const _lambda = [&] (font_id_type const &_fid) {
		get_font (_fid).padding (_rect);
	};

	if (_sfid != "*") {
		_lambda (clifw::universal_cast<font_id_type> (_sfid));			
		return *this;
	}

	for_each_font_id (_lambda, true);
	return *this;
}


lirc::app &lirc::app::add_char (font_id_type const &_fid, char_type const &_chr) {
	auto &_fref = get_font (_fid);
	if (_fref.is_native ()) {
		return *this;	
	}
	_working_font_set.insert (_fid);
	_fref.charset ().insert (_chr);
	return *this;
}

lirc::app &lirc::app::add_char (font_id_type const &_fid, charset_type const &_chrl) {
	auto &_fref = get_font (_fid);
	if (_fref.is_native ()) {
		return *this;
	}
	_working_font_set.insert (_fid);
	_fref.charset ().insert (_chrl.begin (), _chrl.end ());
	return *this;
}

lirc::app &lirc::app::for_each_font_id (std::function<void (font_id_type const &_fid)> const &_ffun, bool _working) {
	for (auto const &_fid : _working ? _working_font_set : _total_font_set) 
		_ffun (_fid);
	return *this;
}

lirc::app &lirc::app::for_each_font (std::function<void (lirc::font_reference &_fref, font_id_type const &_fid)> const &_ffun, bool _working) {
	for (auto const &_fid : _working ? _working_font_set : _total_font_set) 
		_ffun (get_font (_fid), _fid);
	return *this;
}

lirc::app &lirc::app::inject_char (font_id_type const &_fid, font_id_type const &_from_fid, charset_type const &_cset) {
	if (_from_fid == _fid)
		return *this;
	_working_font_set.insert (_fid);
	for (auto const &_cid: _cset) 
		inject_char (_fid, _from_fid, _cid);
	return *this;
}

lirc::app &lirc::app::inject_char (font_id_type const &_fid, font_id_type const &_from_fid, char_type const &_cid) {
	if (_from_fid == _fid)
		return *this;
	_working_font_set.insert (_fid);
	auto &_fref = get_font (_fid);
	_fref.inject_glyph (_cid, 
		glyph_reference (_cid, _from_fid, _fref.size ()));	
	return *this;
}

lirc::app &lirc::app::inject_bitmap (
    font_id_type const &_fid, 
    char_type const &_chr, 
    std::string const &_path, 
    std::int_fast32_t  const &_baseline) 
{
	_working_font_set.insert (_fid);
	auto &_fref = get_font (_fid);
	_fref.inject_glyph (_chr,
		glyph_reference (_path, _fref.size (), _baseline));
	return *this;
}

lirc::app::font_id_type lirc::app::font_remap_id (font_id_type const &_fid) const try {	
	return _font_map.at (_fid);
}
catch (std::out_of_range const &) {
	throw lirc::bad_reference_exception ("Referencing a font (%d) not marked for output", _fid);
}

lirc::app::string_type lirc::app::remap_string (font_id_type const &_fid, string_type const & _str) const {
	auto const &_fref = get_font (_fid);
	auto const &_cmap = _fref.charmap () ;
	auto _temp = _str;
	std::transform (_str.begin (), _str.end (), _temp.begin (), [&] (char_type _chr) {
		try {
			return _cmap.at (_chr);
		}
		catch (std::out_of_range const &) {
			throw bad_charmap_exception ("Error while remapping translation string (character 0x%04X not found in charmap).", _chr);
		}
	});	
	return _temp;
}

lirc::app &lirc::app::fixup_baseline_shift_glyphs (
    std::string const &s_fid_, 
    std::string const &s_shift_, 
    std::list<std::string> const &s_glyphs_) 
{
    auto shift_ = clifw::universal_cast<std::int32_t> (s_shift_);
    auto fid_   = clifw::universal_cast<font_id_type> (s_fid_);
    auto &fref_  = get_font (fid_);
    for (auto const &g : s_glyphs_) {
        auto glyph_ = clifw::universal_cast<char_type> (g);
        fref_.baseline_shift_glyph (shift_, glyph_); 
    }
    return *this;
}


lirc::app &lirc::app::fixup_per_glyph_padding (
    std::string const &s_fid_, 
    std::string const &s_pleft_, 
    std::string const &s_pright_, 
    std::list<std::string> const &s_glyphs_) 
{
    auto pleft_  = clifw::universal_cast<std::int32_t> (s_pleft_);
    auto pright_ = clifw::universal_cast<std::int32_t> (s_pright_);
    auto fid_    = clifw::universal_cast<font_id_type> (s_fid_);
    auto &fref_  = get_font (fid_);
    for (auto const &g : s_glyphs_) {
        auto glyph_ = clifw::universal_cast<char_type> (g);
        fref_.local_padding (glyph_, bitmapio::make_rect (0, pright_, 0, pleft_));
    }
    return *this;
}


