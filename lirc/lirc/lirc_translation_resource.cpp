#include "lirc_translation_resource.h"
#include "lirc_tilsv.h"
#include "lirc_spasv.h"
#include "lirc_reference_index.hpp"

#include <string>
#include <filesystem>
using namespace lirc ;

lirc::translation_resource::translation_resource (std::string const &_path):
	_item_collection (),
	_name (std::tr2::sys::path (_path).basename ())
{	
	auto _items = clifw::helper::file_type (_path) == ".tilsv" ? 
		lirc::tilsv_file<char_type> (_path).items () :
		lirc::spasv_file<char_type> (_path).items () ;

	for (auto const &_it : _items) {
		try {
			auto _element = translation_element_type (
				clifw::universal_cast<translation_resource::font_id_type>(_it.at (0)), 
				_it.size () > 1 ? _it [1] : string_type ());
			_item_collection.push_back (_element);
			auto const &_charset = _element.charset ();
			_font_id_set.insert (_element.font_id ());
			_font_charset_map [_element.font_id ()].insert (
				_charset.begin (), _charset.end ());
			reference_index<char_type>::get ()
				.register_reference (
					_charset.begin (), 
					_charset.end (), 
					_it.line_reference ());
		}
		catch (...) {
			auto const &_lref = _it.line_reference ();
			throw bad_font_id_exception (
				"Bad font id on '%s:%d'", 
				std::get<0>(_lref).c_str (), 
				std::get<1>(_lref));
		}

	}
	
}

lirc::translation_resource::charset_type const &
	lirc::translation_resource::font_charset (unsigned i) const
{
	return _font_charset_map.at (i);
}

lirc::translation_resource::font_set_type const &
	lirc::translation_resource::font_set () const 
{
	return _font_id_set;
}

lirc::translation_resource::item_collection_type const &lirc::translation_resource::items () const {
	return _item_collection;
}

lirc::translation_resource::item_collection_type &lirc::translation_resource::items () {
	return _item_collection;
}

std::string const &lirc::translation_resource::name () const {
	return _name ;
}