#ifndef __CLIFW_EXCEPTION_H__
#define __CLIFW_EXCEPTION_H__

#include <string>
#include <stdexcept>
#include "clifw_logger.h"

namespace clifw {

	
	struct exception : std::exception {	

		exception (std::string const & reason, int code = -1, int type = logger::FATAL) throw () ;
		virtual ~exception () throw () ; 
		virtual char const *what () const throw ();

		virtual std::string const &reason () const throw () ;
		virtual std::string const &formatted () const throw () ;
		virtual int const &type () const throw () ;
		virtual int const &code () const throw () ;
	private:
		std::string _formatted;
		std::string _reason;
		int _code;
		int _type;
	};	

	template <int Code, int Type = logger::FATAL> 
		struct exception_template : exception {
			/*
			exception_template (std::string const &reason) 
				exception (reason, Code, Type)
			{}*/

			template <typename... Args>
				exception_template (std::string const &fmt, Args... args) :
					exception (helper::sprintf_format_string (fmt, args...), Code, Type)
				{}
		};
	
	typedef exception_template<-1> undefined_option_exception ;
	typedef exception_template<-2> undefined_argument_exception ;
	typedef exception_template<-3> value_expected_exception ;
	typedef exception_template<-4> value_unexpected_exception ;
	typedef exception_template<-5> option_required_exception;
	typedef exception_template<-6> too_few_arguments_exception;

}

#endif