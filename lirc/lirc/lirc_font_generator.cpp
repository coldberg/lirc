#ifndef __LIRC_FONT_GENERATOR_HPP__
#define __LIRC_FONT_GENERATOR_HPP__

#include "lirc_font_generator.hpp"
#include "lirc.h"

namespace lirc {
	namespace font {			
	
		template <typename FontType>
			generator<FontType>::generator (glyph_renderer_type &inGRen_, font_precursor_type const &inFprec_, app &inApp_):
				glyph_renderer_		(inGRen_),
				font_precursor_		(inFprec_),
				app_				(inApp_),
				min_width_			(inApp_.option ("font-minimum-width").value ()),
				ascent_dont_care_	(inApp_.option ("no-global-ascent").present ())
			{
			}

		template <typename FontType>
			generator<FontType> &generator<FontType>::write (std::string const &_path) {
				#pragma pack(push, 1)
				struct OS1ROMFONT_{
					std::uint16_t maxwidth;            /*0x00: max width in pixels */
					std::uint16_t height;              /*0x02: height in pixels */
					std::uint16_t ascent;              /*0x04: ascent (baseline) height */
					std::uint16_t firstchar;           /*0x06: first character code in bitmap */
					std::uint16_t defaultchar;         /*0x08: default character */
					std::uint16_t size;                /*0x0A: size in characters (# of characters) */
					std::uint32_t bitmap_addr;         /*0x0C: address of 16-bit right pad bitmap data */
					std::uint32_t width_addr;          /*0x10: character widths or 0 if fixed characters */
					std::uint32_t offset_addr;         /*0x14: offsets into bitmap data (offset in words). If 0 then calculate. */
					std::uint32_t bitmap_size;         /*0x18: # of words in bitmap data */
				};
				#pragma pack(pop)

				std::ofstream _ftd (_path, std::ios::binary);

				auto const write = [&](void const * _ptr, std::size_t _size) -> std::ostream &{
					return _ftd.write (reinterpret_cast<char const *>(_ptr), _size);				
				};

				auto const &_glyphs			= font_precursor_.glyphs ();
				auto const _first_glyph		= font_precursor_.first_glyph () ;
				auto const _last_glyph		= font_precursor_.last_glyph () ;
				auto const _default_glyph	= font_precursor_.default_glyph () ;	
				auto const _padding_rect	= font_precursor_.padding ();
                auto const &_fref           = font_precursor_.fref ();
       
				std::int32_t top_ = 0;				
				std::int32_t bot_ = 0;
				std::int32_t lft_ = 0;				
				std::int32_t rit_ = 0;

				std::vector<std::reference_wrapper<const glyph_bitmap>> _glyph_bitmaps;

				for (auto const &_glyph : _glyphs) {
					auto const &_gbmpref = glyph_renderer_.get_glyph_bitmap (_glyph);
					_glyph_bitmaps.push_back (std::cref (_gbmpref));		
					auto &&rect_ = _gbmpref.rect ();
					top_ = std::min (top_, rect_.top	() - _padding_rect.top		());
					bot_ = std::max (bot_, rect_.bottom () + _padding_rect.bottom	());
					lft_ = std::min (lft_, rect_.left	() - _padding_rect.left		());
					rit_ = std::max (rit_, rect_.right	() + _padding_rect.right	());
				}

				std::list<glyph_buffer_type>	_chunks;
				std::vector<width_type>			_widths;
				std::vector<offset_type>		_offsets;

				OS1ROMFONT_ _hdr;

				auto const width_ = rit_ - lft_;
				auto const height_ = bot_ - top_;

				_hdr.maxwidth		= (std::uint16_t (width_));
				_hdr.height			= (std::uint16_t (height_));
				_hdr.ascent			= (std::uint16_t (-top_));
				_hdr.firstchar		= (std::uint16_t (_first_glyph));
				_hdr.defaultchar	= (std::uint16_t (_default_glyph));
				_hdr.size			= (std::uint16_t (_glyphs.size ()));
				_hdr.bitmap_addr	= (std::uint32_t (bitmapio::detail::align_ceil (
					sizeof (_hdr), sizeof (word_type))));

				if (ascent_dont_care_)
					_hdr.ascent	= _hdr.height;

				offset_type _offset = 0;				

				for (auto const &_git: _glyph_bitmaps) {										
					auto &&gBmp_ = build_glyph (top_, bot_, _git.get (), _padding_rect, 0);
					_chunks.emplace_back (gBmp_.container ());
					_widths.emplace_back (endian_fix (width_type (gBmp_.width ())));
					_offsets.emplace_back (endian_fix (_offset));
					_offset += offset_type (_chunks.back ().size ());
				}

				_hdr.width_addr = (std::uint32_t (_hdr.bitmap_addr + _offset));
				_hdr.offset_addr = (std::uint32_t (_hdr.width_addr + std::uint32_t (_widths.size ())));
				_hdr.bitmap_size = _offset;

				if (_hdr.height < 3)
					throw bad_command_exception ("Invalid vertical padding values, can't have height smaller then 3.");

				endian_fix_inplace (_hdr.maxwidth);
				endian_fix_inplace (_hdr.height);
				endian_fix_inplace (_hdr.ascent);
				endian_fix_inplace (_hdr.firstchar);
				endian_fix_inplace (_hdr.defaultchar);
				endian_fix_inplace (_hdr.size);
				endian_fix_inplace (_hdr.bitmap_addr);
				endian_fix_inplace (_hdr.width_addr);
				endian_fix_inplace (_hdr.offset_addr);
				endian_fix_inplace (_hdr.bitmap_size);

				write (&_hdr, bitmapio::detail::align_ceil (
					sizeof (_hdr), sizeof (word_type)) * sizeof (word_type));

				for (auto const &_chunk: _chunks) {
					write (&_chunk [0], _chunk.size () * sizeof (_chunk [0]));
				}
				write (&_widths [0], _widths.size () * sizeof (_widths [0]));
				write (&_offsets [0], _offsets.size () * sizeof (_offsets [0]));
 				return *this;
			}

		template <typename FontType>			
			typename generator<FontType>::glyph_bitmap_type 
				generator<FontType>::build_glyph (
					std::int32_t const &top_, 
					std::int32_t const &bot_, 
					glyph_bitmap const &inBitmap_, 
					bitmapio::rect<std::int32_t> const &inPadding_,
                    std::int32_t const &inShift_)
			{
				auto const oldRect_ = inBitmap_.rect ();
				
				bitmapio::rect<std::int32_t> newRect_ {
					{oldRect_.left  () - inPadding_.left  (), top_},
					{oldRect_.right () + inPadding_.right (), bot_}};
				if (newRect_.width () < 3) {
					auto const xtraPad_ =  3 - newRect_.width ();
					newRect_ = bitmapio::rect<std::int32_t> {
						{newRect_.left	(),			   newRect_.top		()},
						{newRect_.right	() + xtraPad_, newRect_.bottom	()}
					};
				}					

				glyph_bitmap_type newBitmap_ (newRect_, 0);
				bitmapio::superimpose (newBitmap_, inBitmap_);
				return newBitmap_;
			}

		template <typename FontType>
			void generate (std::string const &inPath_, typename generator<FontType>::font_precursor_type const &inPrecursor_, 
				app &inApp_, typename generator<FontType>::glyph_renderer_type &inRender_) 
			{
				generator<FontType> (inRender_, inPrecursor_, inApp_).write (inPath_);
			}
		
		template struct generator<type_f01>;
		template struct generator<type_f08>;
		template struct generator<type_ftd>;

		template void generate<type_f01> (
				std::string const &inPath_, 
				generator<type_f01>::font_precursor_type const &inPrecursor_, 
				app &inApp_, 
				generator<type_f01>::glyph_renderer_type &inRender_);
		template void generate<type_f08> (
				std::string const &inPath_, 
				generator<type_f08>::font_precursor_type const &inPrecursor_, 
				app &inApp_, 
				generator<type_f08>::glyph_renderer_type &inRender_);
		template void generate<type_ftd> (
				std::string const &inPath_, 
				generator<type_ftd>::font_precursor_type const &inPrecursor_, 
				app &inApp_, 
				generator<type_ftd>::glyph_renderer_type &inRender_);
	}
}

#endif