#include "bitmapio_size.h"

namespace bitmapio {
	template <typename T>
		size<T>::size (T const &_w, T const &_h) :
			coord (_w, _h)
		{}

	template <typename T>
		T size<T>::width () const {
			return x ();
		}

	template <typename T>
		T size<T>::height () const{
			return y ();
		}

	template <typename T>
		T size<T>::area () const{
			return width () * height () ;
		}

	template struct size<std::int8_t>;
	template struct size<std::uint8_t>;
	template struct size<std::int16_t>;
	template struct size<std::uint16_t>;
	template struct size<std::int32_t>;
	template struct size<std::uint32_t>;
	template struct size<std::int64_t>;
	template struct size<std::uint64_t>;
	template struct size<float>;
	template struct size<double>;
}