#include "bitmapio_coord.h"

namespace bitmapio {			
	template<typename T>
		coord<T>::coord (T x, T y) :
			x_(x), y_(y)
		{}

	template<typename T>
		coord<T>::coord () :
			x_(T ()),
			y_(T ())
		{}

	template<typename T>		
		bool coord<T>::operator == (coord<T> const &other_) const {
			return x_ == other_.x_ && y_ == other_.y_ ;
		}

	template<typename T>		
		bool coord<T>::operator != (coord<T> const &other_) const {
			return x_ != other_.x_ && y_ != other_.y_ ;
		}

	template<typename T>		
		coord<T> coord<T>::operator + (coord<T> const &other_) const {
			return {x_ + other_.x_, y_ + other_.y_};
		}

	template<typename T>		
		coord<T> coord<T>::operator - (coord<T> const &other_) const {
			return {x_ - other_.x_, y_ - other_.y_};
		}

	template<typename T>		
		coord<T> coord<T>::operator * (coord<T> const &other_) const {
			return {x_ * other_.x_, y_ * other_.y_};
		}

	template<typename T>		
		coord<T> coord<T>::operator / (coord<T> const &other_) const {
			return {x_ / other_.x_, y_ / other_.y_};
		}

	template<typename T>		
		coord<T> coord<T>::operator % (coord<value_type> const &other_) const {
			using detail::mod_;
			return {mod_ (x_, other_.x_), mod_ (y_, other_.y_)};
		}

	template<typename T>		
		T const &coord<T>::x () const {
			return x_;
		}

	template<typename T>		
		T const &coord<T>::y () const {
			return y_;			
		}

	template struct coord<std::int8_t>;
	template struct coord<std::uint8_t>;
	template struct coord<std::int16_t>;
	template struct coord<std::uint16_t>;
	template struct coord<std::int32_t>;
	template struct coord<std::uint32_t>;
	template struct coord<std::int64_t>;
	template struct coord<std::uint64_t>;

	template struct coord<float>;
	template struct coord<double>;
}