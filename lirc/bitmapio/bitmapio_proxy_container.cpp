#include <cstdint>
#include "bitmapio_proxy_container.h"

namespace bitmapio {

	template <typename T>
		proxy_container<T>::proxy_container (iterator const inBegin_, iterator const inEnd_):
			begin_ (inBegin_),
			end_ (inEnd_)
		{
			int i = 0;
		}

	template <typename T>
		proxy_container<T>::proxy_container (proxy_container const &inCont_) :
			begin_ (inCont_.begin_),
			end_ (inCont_.end_)
		{
			int i = 0;
		}

	template <typename T>
		typename proxy_container<T>::iterator 
			proxy_container<T>::begin () const 
		{
			return begin_;
		}

	template <typename T>
		typename proxy_container<T>::iterator 
			proxy_container<T>::end () const 
		{
			return end_;
		}

	template <typename T>
		std::size_t proxy_container<T>::size () const {
			return end_ - begin_;
		}

	template <typename T>
		typename proxy_container<T>::value_type const &
			proxy_container<T>::operator [] (std::size_t const index_) const 
		{
			auto &&size_ = size ();
			if (index_ >= size_) {
				throw std::runtime_error ("Bounds check error " __FUNCTION__);
			}
			return begin_ [index_];
		}

	template <typename T>
		typename proxy_container<T>::value_type &
			proxy_container<T>::operator [] (std::size_t const index_) 
		{
			auto &&size_ = size ();
			if (index_ >= size_) {
				throw std::runtime_error ("Bounds check error " __FUNCTION__);
			}
			return begin_ [index_];
		}

	template struct proxy_container<std::int8_t>;
	template struct proxy_container<std::uint8_t>;
	template struct proxy_container<std::int16_t>;
	template struct proxy_container<std::uint16_t>;
	template struct proxy_container<std::int32_t>;
	template struct proxy_container<std::uint32_t>;
	template struct proxy_container<std::int64_t>;
	template struct proxy_container<std::uint64_t>;
}