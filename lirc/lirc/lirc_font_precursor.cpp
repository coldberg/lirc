#include "lirc_font_precursor.h"
#include <iostream>
#include "../clifw/clifw_logger.h"

using namespace lirc;

font_precursor::font_precursor (
	font_id_type const &_fid, 
    font_reference const &_vfref) :
    _fref (_vfref)
{
	for (auto const &_cid: _vfref.charset ()) {
		try {
			_glyphs.push_back (_vfref.patches ().at (_cid));
		}
		catch (...) {
			_glyphs.push_back (glyph_reference (_cid, _fid, _vfref.height ()));
		}
	}	
	if (_vfref.charmap ().size () != _glyphs.size ())
		throw std::logic_error ("Encountered an error while creating glyph map for font generation");
}

font_precursor::glyph_collection_type const &font_precursor::glyphs () const {
	return _glyphs;
}

font_precursor::glyph_collection_type &font_precursor::glyphs () {
	return _glyphs;
}

font_precursor::char_type const &font_precursor::first_glyph () const {
	return _fref.charmap ().begin ()->second;
}

font_precursor::char_type const &font_precursor::last_glyph () const {
	return _fref.charmap ().rbegin ()->second;
}

font_precursor::char_type const &font_precursor::default_glyph () const {
	return _fref.default_char ();
}

font_precursor::rect_type const &font_precursor::padding () const {
	return _fref.padding ();
}

font_reference const &font_precursor::fref () const {
    return _fref;
}
