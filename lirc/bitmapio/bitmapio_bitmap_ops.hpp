#ifndef __BITMAPIO_BITMAP_OPS_H__
#define __BITMAPIO_BITMAP_OPS_H__

#include "bitmapio_coord_ops.hpp"

namespace bitmapio {

	template <typename BitmapType, typename Callback> 
		inline void each_pixel (BitmapType const &bitmap_, Callback const &callback_) {
			typedef typename BitmapType::coord_type coord_type;
			each_coord (bitmap_.rect (), 
				[&] (coord_type const &coord_) {
					callback_ (coord_, bitmap_.get (coord_));
				}
			);
		}

	template <typename BitmapType, typename Callback> 
		inline void transform_pixels (BitmapType &bitmap_, Callback const &callback_) {
			typedef typename BitmapType::coord_type coord_type;
			each_coord (bitmap_.rect (), 
				[&] (coord_type const &coord_) {
					bitmap_.set (coord_, callback_ (coord_, bitmap_.get (coord_)));
				}
			);
		}

	template <typename OutBitmapType, typename InBitmapType, typename CallbackType>
		inline void superimpose (
				OutBitmapType &outBitmap_, 
				InBitmapType const &inBitmap_, 
				CallbackType const &inCallback_)
		{
			typedef typename OutBitmapType::coord_type coord_type;
			typedef typename OutBitmapType::value_type value_type;
			transform_pixels (outBitmap_, [&] (coord_type const &inCoord_, value_type const &inValue_) {
				return inCallback_ (inCoord_, inValue_, inBitmap_.get (inCoord_));
			});
		}

	template <typename OutBitmapType, typename InBitmapType>
		inline void superimpose (
				OutBitmapType &outBitmap_, 
				InBitmapType const &inBitmap_)
		{
			typedef typename OutBitmapType::coord_type coord_type;
			typedef typename OutBitmapType::value_type value_type;			
			superimpose (outBitmap_, inBitmap_, [] (
				coord_type const &inCoord_, 
				value_type const &inA_, 
				value_type const &inB_) -> value_type 
			{
				return inA_ || inB_;
			});
		}


	template <typename BitmapOutType, typename BitmapInAType, typename BitmapInBType, typename CallbackType>
		inline BitmapOutType combine (BitmapInAType const &inA_, BitmapInBType const &inB_, CallbackType const &inCallback_) {			
			typedef BitmapOutType::coord_type coord_type;
			typedef BitmapOutType::value_type value_type;
			BitmapOutType outBmp_ (inA_.rect () | inB_.rect ());
			superimpose (outBmp_, inA_);
			superimpose (outBmp_, inB_, inCallback_);
			return outBmp_;
		}

	template <typename BitmapOutType, typename BitmapInAType, typename BitmapInBType>
		inline BitmapOutType combine (BitmapInAType const &inA_, BitmapInBType const &inB_) {			
			typedef BitmapOutType::coord_type coord_type;
			typedef BitmapOutType::value_type value_type;
			return combine (inA_, inB_, [] (
				coord_type const &inCoord_, 
				value_type const &inA_, 
				value_type const &inB_) 
			{
				return inA_ || inB_ ;
			});
		}

    template <typename BitmapInType>
        inline typename BitmapInType::rect_type 
            bounding_box (BitmapInType const &inBitmap_) 
        {
			typedef typename BitmapInType::rect_type  rect_type;
			typedef typename BitmapInType::coord_type coord_type;
			typedef typename BitmapInType::value_type value_type;

			auto &&rect_ = inBitmap_.rect ();
			int xmin_ = rect_.right ();
			int xmax_ = rect_.left ();
			int ymin_ = rect_.bottom ();
			int ymax_ = rect_.top ();
                
			each_pixel (inBitmap_, [&xmin_, &xmax_, &ymin_, &ymax_] (
				coord_type const &inCoord_, 
				value_type const &inValue_)
			{
				if (!inValue_)
					return;
				xmin_ = std::min (xmin_, inCoord_.x ());
				xmax_ = std::max (xmax_, inCoord_.x ());
				ymin_ = std::min (ymin_, inCoord_.y ());
				ymax_ = std::max (ymax_, inCoord_.y ());
			});

            return rect_type {
                {xmin_ + 0, ymin_ + 0}, 
                {xmax_ + 1, ymax_ + 1}
            };
        }

	template <typename BitmapOutType, typename BitmapInType, typename InRectType>
		inline BitmapOutType crop (BitmapInType const &inBitmap_, InRectType const &newRect_) {			
			BitmapOutType out_ (newRect_, 0);
			superimpose (out_, inBitmap_);
			return out_;
		}

	template <typename BitmapOutType, typename BitmapInType>		
        inline BitmapOutType crop (BitmapInType const &inBitmap_) {
            return crop<BitmapOutType> (inBitmap_, bounding_box (inBitmap_));
		}
        
}

#endif